# CKI Tools

All the command line tools the CKI team uses.

## Tests

To run the tests, just type `tox`. That should take care of installing
all dependencies needed for it. However, you have to make sure you
install the package `krb5-devel` (with `dnf`, `yum`, or similar).

You can also run tests in a podman container via

```shell
podman run --pull=newer --rm -it --volume .:/code --workdir /code quay.io/cki/cki-tools:production tox
```

To use the `kcidb` module, use the `kcidb` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[kcidb]
```

To summarize a couple of advanced options for `cki_lint.sh`:

- `CKI_PYTEST_ARGS`: Limit which tests to run, e.g. run only tests containing
  "test_update" within "tests/test_cki_tools_service_metrics.py".
- `CKI_DISABLED_LINTERS`: Select which linters to skip, e.g. skip markdown linting
- `PACKAGE_pip_url`: Override which version of any dependency before running tests,
  e.g. use a commit from the MR #273 for cki-lib, with the "psql" extra requirements.

```shell
 podman run --pull=newer --rm -it --volume .:/code:Z --workdir /code \
   --env TOX_OVERRIDE="testenv.passenv+=CKI_PYTEST_ARGS,CKI_DISABLED_LINTERS,*_PIP_URL" \
   --env CKI_PYTEST_ARGS="tests/test_cki_tools_service_metrics.py -k test_update" \
   --env CKI_DISABLED_LINTERS="markdownlint" \
   --env cki_lib_pip_url="git+https://gitlab.com/cki-project/cki-lib@refs/merge-requests/273/merge#egg=cki_lib[psql]" \
   quay.io/cki/cki-tools:production tox
```

## KCIDB Image

An extra image is built to run the KCIDB module.
This image is used on OpenShift pods, and contains `kcidb` extra dependencies.

`quay.io/cki/kcidb:production`

## Detailed documentation of Python tools

| Tool                                            | Detailed documentation                                                                 |
|-------------------------------------------------|----------------------------------------------------------------------------------------|
| `cki.beaker_tools.broken_machines`              | [documentation](documentation/README.cki.beaker_tools.broken_machines.md)              |
| `cki.cki_tools.amqp_bridge`                     | [documentation](documentation/README.cki.cki_tools.amqp_bridge.md)                     |
| `cki.cki_tools.datawarehouse.issue_maintenance` | [documentation](documentation/README.cki.cki_tools.datawarehouse.issue_maintenance.md) |
| `cki.cki_tools.get_kernel_headers`              |                                                                                        |
| `cki.cki_tools.gitlab_ci_bot`                   | [documentation](documentation/README.cki.cki_tools.gitlab_ci_bot.md)                   |
| `cki.cki_tools.gitlab_sso_login`                | [documentation](documentation/README.cki.cki_tools.gitlab_sso_login.md)                |
| `cki.cki_tools.install_dependencies`            | [documentation](documentation/README.cki.cki_tools.install_dependencies.md)            |
| `cki.cki_tools.k8s_event_listener`              | [documentation](documentation/README.cki.cki_tools.k8s_event_listener.md)              |
| `cki.cki_tools.krb_ticket_refresher`            |                                                                                        |
| `cki.cki_tools.orphan_hunter`                   | [documentation](documentation/README.cki.cki_tools.orphan_hunter.md)                   |
| `cki.cki_tools.orphan_hunter_ec2`               | [documentation](documentation/README.cki.cki_tools.orphan_hunter_ec2.md)               |
| `pipeline_data_gen_tree_doc`                    | [documentation](documentation/README.cki_tools_pipeline_data_gen_tree_doc.md)          |
| `cki.cki_tools.retrigger`                       | [documentation](documentation/README.cki.cki_tools.retrigger.md)                       |
| `cki.cki_tools.refresh_dogtag_certificates`     | [documentation](documentation/README.cki.cki_tools.refresh_dogtag_certificates.md)     |
| `cki.cki_tools.repo_manager`                    | [documentation](documentation/README.cki.cki_tools.repo_manager.md)                    |
| `cki.cki_tools.select_kpet_tree`                | [documentation](documentation/README.cki.cki_tools.select_kpet_tree.md)                |
| `cki.cki_tools.service_metrics`                 | [documentation](documentation/README.cki.cki_tools.service_metrics.md)                 |
| `cki.cki_tools.sync_files`                      | [documentation](documentation/README.cki.cki_tools.sync_files.md)                      |
| `cki.cki_tools.sync_polarion_id`                | [documentation](documentation/README.cki_tools.sync_polarion_id.md)                    |
| `cki.cki_tools.update_ystream_composes`         | [documentation](documentation/README.cki_tools.update_ystream_composes.md)             |
| `cki.cki_tools.webhook_receiver`                | [documentation](documentation/README.cki.cki_tools.webhook_receiver.md)                |
| `cki.cki_tools.yaml`                            | [documentation](documentation/README.cki.cki_tools.yaml.md)                            |
| `cki.deployment_tools.deployment_bot`           | [documentation](documentation/README.cki.deployment_tools.deployment_bot.md)           |
| `cki.deployment_tools.gitlab_codeowners_config` | [documentation](documentation/README.cki.deployment_tools.gitlab_codeowners_config.md) |
| `cki.deployment_tools.gitlab_repo_config`       |                                                                                        |
| `cki.deployment_tools.gitlab_runner_config`     | [documentation](documentation/README.cki.deployment_tools.gitlab_runner_config.md)     |
| `cki.deployment_tools.grafana`                  | [documentation](documentation/README.cki.deployment_tools.grafana.md)                  |
| `cki.deployment_tools.secrets`                  | [documentation](documentation/README.cki.deployment_tools.secrets.md)                  |
| `cki.deployment_tools.render`                   | [documentation](documentation/README.cki.deployment_tools.render.md)                   |
| `cki.kcidb.adjust_dumpfiles`                    |                                                                                        |
| `cki.kcidb.beaker_to_kcidb`                     |                                                                                        |
| `cki.kcidb.datawarehouse_submitter`             | [documentation](documentation/README.cki.kcidb.datawarehouse_submitter)                |
| `cki.kcidb.datawarehouse_umb_submitter`         |                                                                                        |
| `cki.kcidb.edit`                                |                                                                                        |
| `cki.kcidb.forward_upstream`                    |                                                                                        |
| `cki.kcidb.get_test_summary`                    | [documentation](documentation/README.cki.kcidb.get_test_summary)                       |
| `cki.kcidb.parser`                              |                                                                                        |
| `cki.triager`                                   | [documentation](documentation/README.cki.triager.md)                                   |
| `cki_tools.autoscaler`                          | [documentation](documentation/README.cki_tools.autoscaler.md)                          |
| `cki_tools.credential_manager`                  | [documentation](documentation/README.cki_tools.credential_manager.md)                  |
| `cki_tools.gating_reporter`                     | [documentation](documentation/README.cki_tools.gating_reporter.md)                     |
| `cki_tools.gitlab_yaml_shellcheck`              | [documentation](documentation/README.cki_tools.gitlab_yaml_shellcheck.md)              |
| `cki_tools.gitrepo_trigger`                     | [documentation](documentation/README.cki_tools.gitrepo_trigger.md)                     |
| `cki_tools.koji_trigger`                        | [documentation](documentation/README.cki_tools.koji_trigger.md)                        |
| `cki_tools.pipeline_herder`                     | [documentation](documentation/README.cki_tools.pipeline_herder.md)                     |
| `cki_tools.slack_bot`                           | [documentation](documentation/README.cki_tools.slack_bot.md)                           |
| `cki_tools.umb_messenger`                       | [documentation](documentation/README.cki_tools.umb_messenger.md)                       |

## Detailed documentation of shell scripts

| Tool                                      | Description                                                                   |
|-------------------------------------------|-------------------------------------------------------------------------------|
| `cki_deployment_acme.sh`                  | [documentation](documentation/README.cki_deployment_acme.md)                  |
| `cki_deployment_clean_docker_images.sh`   | [documentation](documentation/README.cki_deployment_clean_docker_images.md)   |
| `cki_deployment_codeowners_mr.sh`         | [documentation](documentation/README.cki_deployment_codeowners_mr.md)         |
| `cki_deployment_git_s3_sync.sh`           | [documentation](documentation/README.cki_deployment_git_s3_sync.md)           |
| `cki_deployment_grafana_backup.sh`        | [documentation](documentation/README.cki_deployment_grafana_backup.md)        |
| `cki_deployment_grafana_mr.sh`            | [documentation](documentation/README.cki_deployment_grafana_mr.md)            |
| `cki_deployment_osp_backup.sh`            | [documentation](documentation/README.cki_deployment_osp_backup.md)            |
| `cki_deployment_pgsql_backup.sh`          | [documentation](documentation/README.cki_deployment_pgsql_backup.md)          |
| `cki_deployment_pgsql_restore.sh`         | [documentation](documentation/README.cki_deployment_pgsql_restore.md)         |
| `cki_tools_git_cache_updater.sh`          | [documentation](documentation/README.cki_tools_git_cache_updater.md)          |
| `cki_tools_kernel_config_updater.sh`      | [documentation](documentation/README.cki_tools_kernel_config_updater.md)      |
| `cki_tools_update_ystream_composes_mr.sh` | [documentation](documentation/README.cki_tools_update_ystream_composes_mr.md) |

<!-- vi: set spell spelllang=en: -->
