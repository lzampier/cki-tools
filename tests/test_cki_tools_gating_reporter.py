"""Tests for OSCI gating messages."""
from importlib import resources
import json
from pathlib import Path
import unittest
from unittest import mock

from cki_lib import yaml
from freezegun import freeze_time

from cki_tools import gating_reporter

from . import assets
from .utils import b64decode_and_decompress


class TestGating(unittest.TestCase):
    """Tests for OSCI gating messages."""

    def test_compress_and_b64encode(self):
        """Test for compress_and_b64encode."""
        test_cases = ['foo', 'bar', 'baz', '123']
        for data in test_cases:
            result = gating_reporter.compress_and_b64encode(data)
            self.assertTrue(isinstance(result, str))
            self.assertEqual(b64decode_and_decompress(result), data)

    def test_process_message(self):
        """Verify ready_to_report messages are handled correctly."""
        cases = (
            ('ready_to_report', {
                'status': 'ready_to_report',
                'object_type': 'checkout',
                'id': 'redhat:123',
            }, True),
            ('build_setups_finished', {
                'status': 'build_setups_finished',
                'object_type': 'checkout',
                'id': 'redhat:123',
            }, True),
            ('unknown status', {
                'status': 'foo',
                'object_type': 'checkout',
                'id': 'redhat:123',
            }, False),
            ('no checkout', {
                'status': 'ready_to_report',
                'object_type': 'build',
                'id': 'redhat:15',
            }, False),
            ('retriggered', {
                'status': 'ready_to_report',
                'object_type': 'checkout',
                'id': 'redhat:123',
                'object': {'data': 'something', 'misc': {'retrigger': True}},
            }, False),
        )

        for description, payload, reports in cases:
            with (self.subTest(description),
                  mock.patch('cki_tools.gating_reporter.report') as mock_report):
                gating_reporter.process_message(body=payload)
                if reports:
                    mock_report.assert_called()
                else:
                    mock_report.assert_not_called()

    def test_unknown_issues(self):
        """Verify unknown issues are filtered correctly."""
        cases = (
            ('fail', 'FAIL', False, False, {'status': 'FAIL', 'waived': False}, 1),
            ('pass', 'FAIL', False, False, {'status': 'PASS', 'waived': False}, 0),
            ('error', 'FAIL', False, False, {'status': 'ERROR', 'waived': False}, 0),
            ('waived', 'FAIL', False, False, {'status': 'FAIL', 'waived': True}, 0),
            ('issue', 'FAIL', True, False, {'status': 'FAIL', 'waived': False}, 0),
            ('regression', 'FAIL', True, True, {'status': 'FAIL', 'waived': False}, 1),
        )

        for description, status, issue, regression, test, expected in cases:
            with self.subTest(description):
                self.assertEqual(len(gating_reporter.unknown_issues(
                    [mock.Mock(id=1, **test)], status,
                    [mock.Mock(is_regression=regression, test_id=1)] if issue else [])), expected)

    def test__status_to_osci_dashboard_format(self):
        """Test for _status_to_osci_dashboard_format."""
        with self.subTest("FAIL returns failed"):
            self.assertEqual('failed', gating_reporter._status_to_osci_dashboard_format('FAIL'))
        with self.subTest("Any other input value than 'FAIL' returns 'passed'"):
            self.assertEqual('passed', gating_reporter._status_to_osci_dashboard_format('PASS'))
            self.assertEqual('passed', gating_reporter._status_to_osci_dashboard_format('foo'))

    def test_make_xunit_string(self):
        """Test for make_xunit_string."""
        self.maxDiff = None
        assets_dir = Path(__file__).resolve().parent / "assets"

        with self.subTest("Return None if no tests"):
            self.assertIsNone(gating_reporter.make_xunit_string([]))

        # Mock the kcidb tests
        with open(assets_dir / "test_results.json", encoding='utf-8') as file:
            kcidb_tests_mocks = [
                mock.MagicMock(**test)
                for test in json.load(file)
            ]
        # Expected xunit XML
        with open(assets_dir / "expected_xunit.xml", encoding='utf-8') as file:
            expected_xunit_xml = file.read().strip().replace('\t', '').replace('\n', '')

        xunit_data = gating_reporter.make_xunit_string(kcidb_tests_mocks)
        xunit_xml = b64decode_and_decompress(xunit_data).strip().replace('\n', '')

        # Main test case
        with self.subTest("Test generated xunit XML matches expected"):
            self.assertEqual(xunit_xml, expected_xunit_xml)

        # Subtest for the case when `make_xunit_string` fails
        with self.subTest("Test make_xunit_string fails and logs error"):
            with self.assertLogs(gating_reporter.LOGGER, 'ERROR') as log:
                xunit_data = gating_reporter.make_xunit_string('foo')
                self.assertIn('An error occurred while generating the xunit string',
                              log.output[-1])
                self.assertIsNone(xunit_data)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @freeze_time('2021-01-01T00:00:00.0+00:00')
    @mock.patch('cki_tools.gating_reporter.SUPPORTED_KOJI_INSTANCES', [{
        'web_url': 'https://brew', 'topic': 'brew-build', 'artifact_type': 'brew-build',
    }])
    def test_gating_messages(self):
        """Tests for the actual messages."""

        cases = (
            ('complete', 'complete', 'osci-complete.yml', 123456, 'https://brew/foo'),
            ('no task id', 'complete', None, None, 'https://brew/bar'),
            ('wrong provenance', 'complete', None, 123456, 'https://koji/baz'),
            ('running', 'running', 'osci-running.yml', 123456, 'https://brew/qux'),
        )

        builds = [
            mock.Mock(architecture='x86_64', id='redhat:11', misc={
                'iid': 12347, 'package_name': 'kernel',
                'kpet_tree_name': 'rhel92-z'}),
            mock.Mock(architecture='s390x', id='redhat:10', misc={
                'iid': 12346, 'package_name': 'kernel-rt',
                'kpet_tree_name': 'rhel92-z', 'debug': True}),
            mock.Mock(architecture='zzz', id='redhat:15', misc={
                'iid': 12346, 'package_name': 'kernel-rt',
                'kpet_tree_name': 'rhel92-z', 'debug': False}),
            mock.Mock(architecture='x86_64', id='redhat:12', misc={
                'iid': 12348, 'package_name': 'kernel',
                'kpet_tree_name': 'rhel92-z', 'debug': True}),
        ]
        tests = [
            mock.Mock(build_id='redhat:10', status='FAIL',
                      waived=False, issue=True, regression=True),
            mock.Mock(build_id='redhat:15', status='PASS', waived=False),
            mock.Mock(build_id='redhat:12', status='ERROR', waived=False),
        ]

        for description, message_type, asset, brew_task_id, web_url in cases:
            checkout = mock.Mock(id='redhat:checkout', misc={
                'scratch': False,
                'kernel_version': '123.notatest',
                'source_package_name': 'source-package-name',
                'iid': 1112,
                'provenance': [{'url': web_url}],
            }, attributes={'contacts': ['joe@example.email']})
            if brew_task_id:
                checkout.misc['brew_task_id'] = brew_task_id
            checkout.manager.api.host = 'https://host'

            with (self.subTest(description),
                  mock.patch('cki_lib.stomp.StompClient.send_message') as send_message,
                  mock.patch('cki_tools.gating_reporter.dw_checkout', return_value=mock.Mock(
                      checkouts=[checkout], builds=builds, tests=tests, issueoccurrences=[])) as dw,
                  mock.patch('cki_tools.gating_reporter.make_xunit_string',
                             return_value='mockedbase64payload')):
                gating_reporter.report(message_type, 'checkout_id')
                dw.assert_called_once_with('checkout_id')
                messages = [c.args[0] for c in send_message.mock_calls]
                if asset is not None:
                    self.assertEqual(messages, yaml.load(
                        contents=resources.files(assets).joinpath(asset).read_text('utf8')))
                else:
                    self.assertEqual(messages, [])
