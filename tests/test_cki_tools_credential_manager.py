"""Tests for credential management."""
import contextlib
from http import HTTPStatus
import os
import pathlib
import tempfile
import typing
import unittest
from unittest import mock

import responses
import yaml

from cki.deployment_tools import secrets
from cki_tools.credential_manager import gitlab
from cki_tools.credential_manager import metrics
from cki_tools.credential_manager import utils


class TestCredentialManager(unittest.TestCase):
    """Tests for credential management."""

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(
        variables: typing.Dict[str, typing.Any],
    ) -> typing.Iterator:
        with tempfile.TemporaryDirectory() as directory, mock.patch.dict(os.environ, {
            'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            pathlib.Path(directory, 'secrets.yml').write_text(
                yaml.safe_dump(variables), encoding='utf8')
            yield

    def test_all_tokens(self) -> None:
        """Test behavior of all_tokens."""
        cases = (
            ('default', ['t1', 't2'], {'t1'}, ['0']),
            ('multiple', ['t1'], {'t1', 't2'}, ['0']),
        )
        for description, tokens, types, expected in cases:
            with self.subTest(description), self._setup_secrets({
                str(i): {'.meta': {'token_type': t}} for i, t in enumerate(tokens)
            }):
                self.assertEqual(utils.all_tokens(types), expected)

    def test_metrics(self) -> None:
        """Test behavior of metrics.process."""
        cases = (
            ('default', [
                {'token_type': 'gitlab_token', 'expires_at': '2000-01-01'},
            ], {
                'cki_token_expires_at{name="0"} 9.466848e+08',
            }),
            ('no expires_at', [
                {'token_type': 'gitlab_token'},
                {'token_type': 'gitlab_token', 'expires_at': '2000-01-01'},
            ], {
                'cki_token_expires_at{name="1"} 9.466848e+08',
            }),
            ('wrong type', [
                {'token_type': 'wrong_type', 'expires_at': '2010-01-01'},
                {'token_type': 'gitlab_token', 'expires_at': '2000-01-01'},
            ], {
                'cki_token_expires_at{name="1"} 9.466848e+08',
            }),
        )
        for description, metas, expected in cases:
            with self.subTest(description), self._setup_secrets({
                str(i): {'.meta': m} for i, m in enumerate(metas)
            }):
                self.assertEqual({
                    m for m in metrics.process().split('\n') if m.startswith('cki_')
                }, expected)

    @responses.activate
    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_gitlab_create(self) -> None:
        """Test behavior of gitlab.create."""
        responses.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
        responses.get('https://instance/api/v4/groups/g', json={'id': 4})
        responses.post('https://instance/api/v4/projects/1/access_tokens', json={
            'id': 3, 'user_id': 2, 'created_at': '2000-01-01', 'expires_at': '2010-01-01',
            'revoked': False, 'active': True, 'token': 'secret',
        })
        responses.post('https://instance/api/v4/groups/4/access_tokens', json={
            'id': 3, 'user_id': 2, 'created_at': '2000-01-01', 'expires_at': '2010-01-01',
            'revoked': False, 'active': True, 'token': 'secret',
        })
        responses.get('https://instance/api/v4/users/2', json={
            'username': 'user'
        })

        cases = (
            ('project token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
            }, {
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('group token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
            }, {
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('unsupported token type', 'token', {
                'token_type': 'bugzilla_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
            }, None),
            ('unsupported gitlab token type', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
                'scopes': ['scope'],
                'token_name': 'name',
            }, None),
        )
        for description, token, meta, expected in cases:
            with self.subTest(description), self._setup_secrets({token: {'.meta': meta}}):
                if expected is None:
                    with self.assertRaises(Exception):
                        gitlab.process('create', token)
                else:
                    gitlab.process('create', token)
                    self.assertEqual(secrets.secret(f'{token}:'), {'value': 'secret'})
                    self.assertEqual(secrets.secret(f'{token}#'), {**meta, **expected})

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_gitlab_update(self) -> None:
        """Test behavior of gitlab.update."""
        cases = (
            ('project token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['scope'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                }
            }, {
                'url': 'https://instance/api/v4/projects/1/members/2',
                'json': {'username': 'user', 'access_level': 10},
            }], {
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',

            }),
            ('group token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['scope'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/groups/4/members/2',
                'json': {'username': 'user', 'access_level': 10},
            }], {
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['scope'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/users/2',
                'json': {'username': 'user'},
            }], {
                'scopes': ['scope'],
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('runner authentication token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'runner_authentication_token',
                'instance_url': 'https://instance',
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'json': {'id': 5, 'token_expires_at': '2020-01-01'},
            }], {
                'token_id': 5,
                'expires_at': '2020-01-01',
            }),
            ('unknown token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'some_token',
            }, [], {}),
        )
        for description, token, meta, url_mocks, expected in cases:
            with self.subTest(description), self._setup_secrets({token: {
                    '.data': {'value': secrets.encrypt('token')}, '.meta': meta,
            }}), responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'GET', **url_mock})
                rsps.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
                rsps.get('https://instance/api/v4/groups/g', json={'id': 4})
                if expected is None:
                    with self.assertRaises(Exception):
                        gitlab.process('update', token)
                else:
                    gitlab.process('update', token)
                    self.assertEqual(secrets.secret(f'{token}#'), {**meta, **expected})

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_gitlab_validate(self) -> None:
        """Test behavior of gitlab.validate."""
        cases = (
            ('project token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True}
            }], True),
            ('invalid project token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('revoked project token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': True, 'active': True}
            }], False),
            ('inactive project token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': False}
            }], False),
            ('group token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], True),
            ('invalid group token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('revoked group token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': True, 'active': True},
            }], False),
            ('inactive group token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': False},
            }], False),
            ('personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], True),
            ('invalid personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('revoked personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': True, 'active': True},
            }], False),
            ('inactive personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': False},
            }], False),
            ('runner authentication token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'runner_authentication_token',
                'instance_url': 'https://instance',
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'json': {'id': 5, 'token_expires_at': '2020-01-01'},
            }], True),
            ('invalid runner authentication token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'runner_authentication_token',
                'instance_url': 'https://instance',
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('unknown token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'some_token',
            }, [], True),
        )
        for description, token, meta, url_mocks, expected in cases:
            with self.subTest(description), self._setup_secrets({token: {
                    '.data': {'value': secrets.encrypt('token')}, '.meta': meta,
            }}), responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'GET', **url_mock})
                if not expected:
                    with self.assertRaises(Exception):
                        gitlab.process('validate', token)
                else:
                    gitlab.process('validate', token)
