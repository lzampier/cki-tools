"""Tests for kpet tree selection."""
import unittest

from cki.cki_tools import select_kpet_tree

CONFIG = """
rhel610-z: '.el6'
rhel74-z: '3.10.0-693..*.el7'
rhel76-z: '3.10.0-957..*.el7'
rhel77-z: '3.10.0-1062..*.el7'
rhel79-z: '3.10.0-1160..*.el7'
rhel8{minor}-z: '.el8_(?P<minor>[124678])'
rhel8: ['.el8_[9]', '.el8(?!_)']
rhel9{minor}-z: '.el9_(?P<minor>[012])'
rhel9: ['.el9_[3456789]', '.el9(?!_)']
"""


class TestSelectKpetTree(unittest.TestCase):
    """Tests for select_kpet_tree module."""

    def test_nvrs(self):
        """Check that the main function works correctly."""
        cases = (
            ('rhel6', '2.6.32-754.500.500.el6', 'rhel610-z'),
            ('rhel6', '2.6.32-0.el7', Exception()),
            ('rhel7', '3.10.0-693.500.500.el7', 'rhel74-z'),
            ('rhel7', '3.10.0-957.500.500.el7', 'rhel76-z'),
            ('rhel7', '3.10.0-1062.500.500.el7', 'rhel77-z'),
            ('rhel7', '3.10.0-1160.500.500.el7', 'rhel79-z'),
            ('rhel7', '3.10.0-1160.11.1.el7.test', 'rhel79-z'),
            ('rhel7', '3.10.0-1161.el7', Exception()),
            ('rhel7', '3.10.1-0.el7', Exception()),
            ('rhel7', '3.10.0-327.500.500.el7', Exception()),
            ('rhel7', '3.10.0-514.500.500.el7', Exception()),
            ('rhel7', '3.10.0-1127.500.500.el7', Exception()),
            ('rhel7-rt', '3.10.0-327.500.500.el7', Exception()),
            ('rhel7-rt', '3.10.0-514.500.500.el7', Exception()),
            ('rhel7-rt', '3.10.0-957.500.500.el7', 'rhel76-z'),
            ('rhel7-rt', '3.10.0-1127.500.500.el7', Exception()),
            ('rhel7-rt', '3.10.0-1160.500.500.el7', 'rhel79-z'),
            ('rhel7-rt', '3.10.0-1161.el7', Exception()),
            ('rhel7-rt', '3.10.1-0.el7', Exception()),
            ('rhel8', '4.18.0-147.500.500.el8_1', 'rhel81-z'),
            ('rhel8', '4.18.0-193.500.500.el8_2', 'rhel82-z'),
            ('rhel8', '4.18.0-500.500.500.el8_4', 'rhel84-z'),
            ('rhel8', '4.18.0-500.500.500.el8_6', 'rhel86-z'),
            ('rhel8', '4.18.0-500.500.500.el8_7', 'rhel87-z'),
            ('rhel8', '4.18.0-500.500.500.el8_8', 'rhel88-z'),
            ('rhel8', '4.18.0-1.500.500.el8', 'rhel8'),
            ('rhel8', '4.18.0-80.500.500.el8_0', Exception()),
            ('rhel8', '4.18.0-240.500.500.el8_3', Exception()),
            ('rhel8', '4.18.0-500.500.500.el8_5', Exception()),
            ('rhel8-rt', '4.18.0-147.500.500.el8_1', 'rhel81-z'),
            ('rhel8-rt', '4.18.0-193.500.500.el8_2', 'rhel82-z'),
            ('rhel8-rt', '4.18.0-500.500.500.el8_4', 'rhel84-z'),
            ('rhel8-rt', '4.18.0-500.500.500.el8_6', 'rhel86-z'),
            ('rhel8-rt', '4.18.0-500.500.500.el8_7', 'rhel87-z'),
            ('rhel8-rt', '4.18.0-500.500.500.el8_8', 'rhel88-z'),
            ('rhel8-rt', '4.18.0-1.500.500.el8', 'rhel8'),
            ('rhel8-rt', '4.18.0-80.500.500.el8_0', Exception()),
            ('rhel8-rt', '4.18.0-240.500.500.el8_3', Exception()),
            ('rhel9', '5.9.0-500.500.500.el9_0', 'rhel90-z'),
            ('rhel9', '5.9.0-500.500.500.el9_1', 'rhel91-z'),
            ('rhel9', '5.9.0-500.500.500.el9_2', 'rhel92-z'),
            ('rhel9', '5.9.0-500.500.500.el9', 'rhel9'),
            ('rhel9', '5.9.0-1.500.500.el9', 'rhel9'),
            ('rhel9', '5.9.0-0.el10', Exception()),
            ('rhel9-rt', '5.9.0-500.500.500.el9_0', 'rhel90-z'),
            ('rhel9-rt', '5.9.0-500.500.500.el9_1', 'rhel91-z'),
            ('rhel9-rt', '5.9.0-500.500.500.el9_2', 'rhel92-z'),
            ('rhel9-rt', '5.9.0-1.500.500.el9', 'rhel9'),
            ('rhel9-rt', '5.9.0-0.el10', Exception()),
        )

        for package_name, kernel_version, expected in cases:
            with self.subTest(f'{package_name}-{kernel_version}'):
                cli_params = [
                    '--package-name', package_name,
                    '--kernel-version', kernel_version,
                    '--nvr-tree-mapping', CONFIG,
                ]
                if isinstance(expected, Exception):
                    self.assertRaises(Exception, select_kpet_tree.main, cli_params)
                else:
                    self.assertEqual(select_kpet_tree.main(cli_params), expected)
