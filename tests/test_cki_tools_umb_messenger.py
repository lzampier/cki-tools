"""Tests for the UMB messenger."""
from importlib import resources
import unittest
from unittest import mock

from cki_lib import yaml
from freezegun import freeze_time
import responses

from cki_tools import umb_messenger

from . import assets


class TestUmbMessenger(unittest.TestCase):
    """Tests for the UMB messenger."""

    maxDiff = None

    def test_process_message(self):
        """Verify messages are sent correctly."""
        cases = (
            ('post_test', {
                'status': 'ready_to_report',
                'object_type': 'checkout',
                'object': {'id': 'redhat:123'},
            }, True),
            ('pre_test', {
                'status': 'build_setups_finished',
                'object_type': 'checkout',
                'object': {'id': 'redhat:123'},
            }, True),
            ('unknown status', {
                'status': 'foo',
                'object_type': 'checkout',
                'object': {'id': 'redhat:123'},
            }, False),
            ('no checkout', {
                'status': 'ready_to_report',
                'object_type': 'build',
                'object': {'id': 'redhat:15'},
            }, False),
            ('retriggered', {
                'status': 'ready_to_report',
                'object_type': 'checkout',
                'object': {'id': 'redhat:123', 'data': 'something', 'misc': {'retrigger': True}},
            }, False),
        )

        for description, payload, reports in cases:
            with (self.subTest(description),
                  mock.patch('cki_tools.umb_messenger.report') as mock_report):
                umb_messenger.process_message(body=payload)
                if reports:
                    mock_report.assert_called_with(description, 'redhat:123')
                else:
                    mock_report.assert_not_called()

    def test_unknown_issues(self):
        """Verify unknown issues are filtered correctly."""
        cases = (
            ('fail', 'FAIL', False, False, {'status': 'FAIL', 'waived': False}, 1),
            ('pass', 'FAIL', False, False, {'status': 'PASS', 'waived': False}, 0),
            ('error', 'FAIL', False, False, {'status': 'ERROR', 'waived': False}, 0),
            ('waived', 'FAIL', False, False, {'status': 'FAIL', 'waived': True}, 0),
            ('issue', 'FAIL', True, False, {'status': 'FAIL', 'waived': False}, 0),
            ('regression', 'FAIL', True, True, {'status': 'FAIL', 'waived': False}, 1),
        )

        for description, status, issue, regression, test, expected in cases:
            with self.subTest(description):
                self.assertEqual(len(umb_messenger.unknown_issues(
                    [mock.Mock(id=1, **test)], status,
                    [mock.Mock(is_regression=regression, test_id=1)] if issue else [])), expected)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch.dict('os.environ', {'DATAWAREHOUSE_URL': 'https://host.url'})
    @freeze_time('2021-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_messages(self):
        """Tests for the actual messages."""
        responses.get('https://host/api/v4/projects/g%2Fp/merge_requests/8', json={
            'iid': 123,
            'description': ('Draft: my mr\nBugzilla: https://bz\nrandom line'
                            'with a Bugzilla: do-not-extract\nBugzilla:'
                            'http://extract-this  \nSigned-off-by: myself\n'
                            'JIRA: https://this.is.jira/123'),
            'labels': ['randomlabel', 'Bugzilla::NeedsReview',
                       'Acks::NeedsReview', 'Subsystem:scsi',
                       'Subsystem:qla2xxx'],
            'work_in_progress': True
        })

        cases = (
            ('pre_test', 'pre_test', [], 'pre-test.yml'),
            ('pre_test', 'pre_test', ['mr'], 'pre-test-mr.yml'),
            ('not valid', 'pre_test', ['invalid'],  None),
            ('no builds', 'pre_test', ['nobuilds'], None),
            ('no output files', 'pre_test', ['nooutput'], None),
            ('post_test', 'post_test', [], 'post-test.yml'),
            ('post_test no tests', 'post_test', ['notests'], 'post-test.yml'),
            ('post_test error', 'post_test', ['error'], 'post-test-error.yml'),
            ('post_test error waived', 'post_test', ['error', 'waived'], 'post-test.yml'),
            ('post_test fail', 'post_test', ['fail'], 'post-test-fail.yml'),
            ('post_test fail waived', 'post_test', ['fail', 'waived'], 'post-test.yml'),
        )

        for description, message_type, deviations, asset in cases:
            checkout = mock.Mock(
                attributes={'contacts': ['joe@example.email']},
                git_repository_branch='main',
                id='redhat:1',
                misc={
                    'kernel_version': '123.test',
                    'source_package_name': 'kernel-source',
                    'iid': 1112,
                    'patchset_modified_files': [
                        {'path': 'list'}, {'path': 'of'}, {'path': 'files'}
                    ],
                },
                valid='invalid' not in deviations,
            )
            checkout.manager.api.host = 'https://host'
            if 'mr' in deviations:
                checkout.misc['related_merge_request'] = {
                    'url': 'https://host/g/p/-/merge_requests/8',
                    'diff_url': 'https://link.to.diff'
                }

            builds = [] if 'nobuilds' in deviations else [
                mock.Mock(
                    architecture='s390x', id='redhat:10',
                    output_files=None if 'nooutput' in deviations else [
                        {'name': 'kernel_package_url', 'url': 'link'},
                    ], misc={
                        'iid': 12346,
                        'package_name': 'kernel-debug',
                        'kpet_tree_name': 'fedora',
                        'debug': True,
                    }),
                mock.Mock(architecture='x86_64', id='redhat:12', output_files=[
                    {'name': 'kernel_package_url', 'url': 'link2'},
                ], misc={
                    'iid': 12348,
                    'package_name': 'kernel',
                    'kpet_tree_name': 'fedora',
                }),
                mock.Mock(architecture='ppc64le', id='redhat:13', output_files=[
                    {'name': 'kernel_package_url', 'url': 'link3'},
                ], misc={
                    'iid': 12347,
                    'package_name': 'kernel',
                    'kpet_tree_name': 'fedora',
                    'testing_skipped_reason': 'unsupported',
                })
            ]

            tests = [] if 'notests' in deviations else [
                mock.Mock(build_id='redhat:15',
                          status='ERROR' if 'error' in deviations else 'pass',
                          waived='waived' in deviations),
                mock.Mock(build_id='redhat:16',
                          status='FAIL' if 'fail' in deviations else 'pass',
                          waived='waived' in deviations),
            ]

            with self.subTest(description), \
                mock.patch('cki_lib.stomp.StompClient.send_message') as send_message, \
                mock.patch('cki_tools.umb_messenger.dw_checkout', return_value=mock.Mock(
                    checkouts=[checkout], builds=builds, tests=tests, issueoccurrences=[])) as dw:
                umb_messenger.report(message_type, 'checkout_id')
                dw.assert_called_once_with('checkout_id')
                if asset is not None:
                    self.assertEqual(send_message.mock_calls[0].args[0], yaml.load(
                        contents=resources.files(assets).joinpath(asset).read_text('utf8')))
                else:
                    send_message.assert_not_called()
