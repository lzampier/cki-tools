"""Tests for update ystream compose in kpet-db."""
from importlib import resources
import pathlib
import unittest
from unittest import mock

import responses

from cki_tools import update_ystream_composes
from tests.assets import update_ystream_composes as assets


class TestYstreamCompose(unittest.TestCase):
    """Tests for update ystream compose in kpet-db."""

    def test_tree_diff(self):
        self.assertEqual(update_ystream_composes.tree_diff(
            resources.files(assets) / 'tree-before.yml',
            diff_path='tree.yml',
            distro_name='RHEL-new.test',
            buildroot_name='BUILDROOT-new.test',
        ), (resources.files(assets) / 'expected.patch').read_text('utf8'))

    @mock.patch('subprocess.run')
    @mock.patch('cki_tools.update_ystream_composes.tree_diff')
    @mock.patch.dict('os.environ', {'CTS_URL': 'https://cts'})
    @responses.activate
    def test_main(self, mock_tree_diff, mock_run):
        """Test for update_ystream_composes."""
        mock_bkr_out = (resources.files(assets) / 'beaker-output.json').read_text('utf8')
        mock_run.return_value = mock.MagicMock(returncode=0, stdout=mock_bkr_out)
        responses.get('https://cts/api/1/composes/RHEL-new.test',
                      json={'children': ['BUILDROOT-new.test']})

        update_ystream_composes.main([
            '--config', (resources.files(assets) / 'config.yaml').read_text('utf8')
        ])
        bkr_params = ['bkr', 'distros-list', '--limit', '1', '--format', 'json', '--name',
                      'RHEL-new', '--tag=CTS_NIGHTLY', '--tag=CTS_integration',
                      '--tag=CTS_beaker-available']
        mock_run.assert_called_with(bkr_params, capture_output=True, check=True)
        mock_tree_diff.assert_called_with(
            pathlib.Path('trees/rhel-test.j2'),
            'trees/rhel-test.j2',
            distro_name='RHEL-new.test',
            buildroot_name='BUILDROOT-new.test',
        )
