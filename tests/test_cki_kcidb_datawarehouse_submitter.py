"""Datawarehouse Submitter tests."""
import contextlib
from http import HTTPStatus
import json
import unittest
from unittest import mock

from cki_lib.kcidb import MAJOR_VERSION
from cki_lib.kcidb import MINOR_VERSION
import responses

from cki.kcidb import datawarehouse_submitter

SCHEMA_VERSION = {"version": {"major": MAJOR_VERSION, "minor": MINOR_VERSION}}


@mock.patch.dict('os.environ', {'DATAWAREHOUSE_URL': 'https://dw-url',
                                'DATAWAREHOUSE_TOKEN_SUBMITTER': 'token'})
class TestDatawarehouseSubmitter(unittest.TestCase):
    """Test cki.kcidb.datawarehouse_submitter."""

    def test_callback(self) -> None:
        """Test callback."""

        cases = [
            ('all-ok', 1),
            ('no-file', 0),
            ('empty', 0),
            ('invalid-json', 0),
            ('invalid-schema', 0),
            ('devel-env', 0),
            ('bad-request', 1),
            ('internal-error', 1),
        ]

        for description, expected in cases:
            with self.subTest(description), \
                    responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
                data = {'json': {**SCHEMA_VERSION,
                                 'checkouts': [{'id': 'redhat:1234', 'origin': 'redhat'}]}}
                dw_status = HTTPStatus.OK
                is_production_or_staging = True
                ctx_check = contextlib.nullcontext()

                if 'no-file' in description:
                    data = {'status': 404}
                if 'empty' in description:
                    data = {}
                if 'invalid-json' in description:
                    data = {'body': json.dumps(data['json'])[:-1]}
                if 'invalid-schema' in description:
                    del data['json']['checkouts'][0]['origin']
                if 'devel-env' in description:
                    is_production_or_staging = False
                if 'bad-request' in description:
                    dw_status = HTTPStatus.BAD_REQUEST
                if 'internal-error' in description:
                    dw_status = HTTPStatus.INTERNAL_SERVER_ERROR
                    ctx_check = self.assertRaises(Exception)

                rsps.get('https://gl/api/v4/projects/p/jobs/1', json={'id': 1, 'name': 'name'})
                rsps.get('https://gl/api/v4/projects/p/jobs/1/artifacts/kcidb_all.json', **data)
                submit_request = rsps.post('https://dw-url/api/1/kcidb/submit', status=dw_status)

                with mock.patch('cki_lib.misc.is_production_or_staging',
                                return_value=is_production_or_staging), ctx_check:
                    datawarehouse_submitter.callback(body={'web_url': 'https://gl/p/-/jobs/1'})

                rsps.assert_call_count(submit_request.url, expected)

    @mock.patch('cki_lib.messagequeue.MessageQueue')
    @mock.patch.dict('os.environ', {'CKI_METRICS_ENABLED': 'false',
                                    'DATAWAREHOUSE_SUBMITTER_ROUTING_KEYS': 'key',
                                    'DATAWAREHOUSE_SUBMITTER_QUEUE': 'queue'})
    def test_main_queue(self, msgqueue) -> None:
        """Test queue processing via the main function."""
        datawarehouse_submitter.main([])
        msgqueue.return_value.consume_messages.assert_called_with(
            'cki.exchange.webhooks', ['key'], datawarehouse_submitter.callback, queue_name='queue')

    @mock.patch('cki.kcidb.datawarehouse_submitter.callback')
    @mock.patch.dict('os.environ', {'CKI_METRICS_ENABLED': 'false'})
    def test_main_single(self, callback) -> None:
        """Test queue processing via the main function."""
        datawarehouse_submitter.main(['job_url'])
        callback.assert_called_with(body={'web_url': 'job_url'})
