"""Test dwobject.py."""
import unittest
from unittest import mock

from datawarehouse import Datawarehouse
from datawarehouse import objects
import responses

from cki.triager import dwobject

from ..utils import mock_attrs


class TestDWObject(unittest.TestCase):
    """Test DWObject."""

    def test_from_attrs(self):
        """Test constructor from_attrs fills attributes as expected."""
        obj = dwobject.from_attrs('checkout', {
            "id": "redhat:1",
            "origin": "redhat",
            "valid": True,
            "misc": {"iid": 1},
            'a': 1,
        })

        self.assertEqual(1, obj.a)
        self.assertEqual("redhat:1", obj.id)
        self.assertEqual("redhat", obj.origin)
        self.assertEqual(True, obj.valid)
        self.assertEqual({"iid": 1}, obj.misc)
        self.assertIsInstance(obj, objects.KCIDBCheckout)

    @responses.activate
    @mock.patch("cki.triager.settings.DW_CLIENT.kcidb.checkouts.get",
                Datawarehouse('http://datawarehouse').kcidb.checkouts.get)
    def test_from_obj_id(self):
        """Test constructor from_obj_id fills fetches from DW as expected."""
        responses.add(
            responses.GET,
            'http://datawarehouse/api/1/kcidb/checkouts/1',
            json={
                "id": "redhat:1",
                "origin": "redhat",
                "valid": True,
                "misc": {"iid": 1},
            })

        obj = dwobject.from_obj_id('checkout', 1)

        self.assertEqual("redhat:1", obj.id)
        self.assertEqual("redhat", obj.origin)
        self.assertEqual(True, obj.valid)
        self.assertEqual({"iid": 1}, obj.misc)
        self.assertIsInstance(obj, objects.KCIDBCheckout)

        with self.assertRaises(Exception):
            dwobject.from_obj_id('invalid', 1)

    @mock.patch('cki.triager.dwobject.from_attrs')
    def test_check_with_dict(self, mocked_constructor):
        """Test Triager.check initializes the DWObject as expected when given a dict.

        Most of the behavior is mocked in here, because it's already tested elsewhere.
        """
        mocked_attrs = mock_attrs()

        mocked_constructor.return_value.finished = False

        types = ["checkout", "build", "test", "testresult"]
        for obj_type in types:
            with self.subTest(obj_type=obj_type):
                mocked_constructor.reset_mock()
                dwobject.from_attrs(obj_type, attrs=mocked_attrs)
                mocked_constructor.assert_called_once_with(obj_type, attrs=mocked_attrs)

    @mock.patch('cki.triager.dwobject.from_obj_id')
    def test_check_with_object_id(self, mocked_constructor):
        """Test Triager.check initializes the DWObject as expected when given an ID.

        Most of the behavior is mocked in here, because it's already tested elsewhere.
        """
        mocked_attrs = "1"

        mocked_constructor.return_value.finished = False

        types = ["checkout", "build", "test", "testresult"]
        for obj_type in types:
            with self.subTest(obj_type=obj_type), \
                    mock.patch(f"cki.triager.settings.DW_CLIENT.kcidb.{obj_type}s.get"):
                mocked_constructor.reset_mock()
                dwobject.from_obj_id(obj_type, obj_id=mocked_attrs)
                mocked_constructor.assert_called_once_with(obj_type, obj_id=mocked_attrs)
