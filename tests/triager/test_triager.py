"""Test __init__.py."""
import contextlib
import json
import unittest
from unittest import mock
from unittest.mock import sentinel

from datawarehouse import Datawarehouse
import pika
import responses

from cki.triager import checkers
from cki.triager import dwobject
from cki.triager import triager

from ..utils import mock_attrs
from ..utils import tear_down_registry

tear_down_registry()


class TestMain(unittest.TestCase):
    """Test __init__.py."""

    @mock.patch('cki.triager.triager.callback')
    @mock.patch('pika.BlockingConnection')
    @mock.patch.dict('os.environ', {'DATAWAREHOUSE_TRIAGER_ROUTING_KEYS': 'key'})
    def test_queue(self, connection, callback_mock):
        """Test listening to messages."""
        msg = {
            'object_type': 'checkout',
            'object': {'id': 1},
            'status': 'new',
        }
        mocked_channel = connection().channel()
        mocked_channel.consume.return_value = [(
            mock.Mock(routing_key='routing_key', delivery_tag='delivery_tag'),
            pika.BasicProperties(),
            json.dumps(msg),
        )]

        triager.main([])

        callback_mock.assert_called_once_with(
            body=msg, routing_key='routing_key', headers=None, ack_fn=None, timeout=False,
        )

    @mock.patch('cki.triager.dwobject.from_attrs', mock.Mock(return_value=sentinel.obj))
    def test_callback(self) -> None:
        """Test the callback only triages the correct objects."""
        cases = (
            ('checkout', 'new', True),
            ('checkout', 'updated', True),
            ('checkout', 'needs_triage', True),
            ('checkout', 'invalid', False),
            ('build', 'new', True),
            ('build', 'updated', True),
            ('build', 'needs_triage', True),
            ('build', 'invalid', False),
            ('test', 'needs_triage', True),
            ('test', 'new', True),
            ('test', 'updated', True),
            ('test', 'invalid', False),
            ('testresult', 'needs_triage', False),
            ('testresult', 'new', False),
            ('testresult', 'updated', False),
            ('testresult', 'invalid', False),
        )
        for object_type, status, expected in cases:
            with self.subTest(f'{object_type}-{status}'), \
                    mock.patch('cki.triager.triager.Triager.check') as check:
                triager.callback(body={
                    'object_type': object_type,
                    'object': {'id': 1},
                    'status': status,
                    'misc': {'issueregex_ids': [1, 2]},
                })
                if expected:
                    check.assert_called_once_with(sentinel.obj, [1, 2])
                else:
                    check.assert_not_called()

    @mock.patch('cki.triager.dwobject.from_attrs', mock.Mock(return_value=sentinel.obj))
    def test_callback_misc(self) -> None:
        """Test the callback handles regex ids from misc correctly."""
        cases = (
            ({'misc': {'issueregex_ids': [1, 2]}}, [1, 2]),
            ({'misc': {'issueregex_ids': []}}, []),
            ({'misc': {'issueregex_ids': None}}, []),
            ({'misc': {}}, []),
            ({'misc': None}, []),
            ({}, []),
        )
        for misc, expected in cases:
            with self.subTest(misc), \
                    mock.patch('cki.triager.triager.Triager.check') as check:
                triager.callback(body={
                    'object_type': 'checkout',
                    'object': {'id': 1},
                    'status': 'new',
                    **misc
                })
                check.assert_called_once_with(sentinel.obj, expected)

    @mock.patch('cki.triager.dwobject.from_obj_id', mock.Mock(return_value=sentinel.obj))
    def test_single(self) -> None:
        """Test main works as expected """
        cases = (
            ('no regex', ['checkout', '1'], []),
            ('with regex', ['checkout', '1', '--regex-id', '7'], ['7']),
        )
        for description, args, expected in cases:
            with self.subTest(description), \
                    mock.patch('cki.triager.triager.Triager.check') as check:
                triager.main(args)
                check.assert_called_once_with(sentinel.obj, expected)

    @mock.patch('cki.triager.triager.IS_PRODUCTION_OR_STAGING', True)
    def test_report_issue(self) -> None:
        """Test report issue."""
        obj = mock.Mock()
        obj.issues.list = mock.Mock(return_value=[mock.Mock(id=3)])
        issues = [
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=1)),
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=2)),
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=2)),
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=3)),
        ]

        triager.Triager().report_issues(issues)
        self.assertEqual(obj.issues.create.mock_calls, [
            mock.call(issue_id=1),
            mock.call(issue_id=2),
        ])

    @staticmethod
    @mock.patch('cki.triager.triager.IS_PRODUCTION_OR_STAGING', False)
    def test_report_issue_dry() -> None:
        """Test report issue. Dry run."""
        obj = mock.Mock()
        obj.issues.list = mock.Mock(return_value=[mock.Mock(id=1)])
        issues = [
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=1)),
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=2)),
        ]

        triager.Triager().report_issues(issues)
        obj.issues.create.assert_not_called()

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('https://dw'))
    def test_check(self):
        """Test Triager.check."""
        cases = (
            ('default', [], checkers.TriageStatus.SUCCESS, True, 200, True, True, False),
            ('default no match', [], checkers.TriageStatus.SUCCESS, True, 200, False, True, False),
            ('with regex ids', [5], checkers.TriageStatus.SUCCESS, True, 200, True, True, False),
            ('regex no match', [5], checkers.TriageStatus.SUCCESS, True, 200, False, False, False),
            ('incomplete', [], checkers.TriageStatus.INCOMPLETE, True, 200, True, False, False),
            ('nonprod', [], checkers.TriageStatus.SUCCESS, False, 200, True, False, False),
            ('not found', [], checkers.TriageStatus.SUCCESS, True, 404, True, True, False),
            ('http error', [], checkers.TriageStatus.SUCCESS, True, 400, True, True, True),
        )
        for description, issueregex_ids, result, prod, status, matches, \
                exp_create, exp_raise in cases:
            with self.subTest(description), \
                    responses.RequestsMock(assert_all_requests_are_fired=False) as rsps, \
                    mock.patch('cki.triager.checkers.triage') as triage, \
                    mock.patch('cki.triager.triager.Triager.report_issues') as report_issues, \
                    mock.patch('cki.triager.triager.IS_PRODUCTION_OR_STAGING', prod):
                triaged = rsps.post('https://dw/api/1/kcidb/checkouts/1/actions/triaged',
                                    json={}, status=status)
                dw_obj = dwobject.from_attrs('checkout', attrs=mock_attrs())
                if matches:
                    triage.return_value = checkers.TriageResult(result, [
                        checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                            mock.Mock(dw_obj=dw_obj), mock.Mock(issue_id=1)),
                        checkers.RegexMatch(checkers.MatchStatus.PARTIAL_MATCH,
                                            mock.Mock(dw_obj=dw_obj), mock.Mock(issue_id=2)),
                        checkers.RegexMatch(checkers.MatchStatus.NO_MATCH,
                                            mock.Mock(dw_obj=dw_obj), mock.Mock(issue_id=3)),
                    ])
                else:
                    triage.return_value = checkers.TriageResult(result, [])
                ctx = self.assertRaises(Exception) if exp_raise else contextlib.nullcontext()
                with ctx:
                    triager.Triager().check(dw_obj, issueregex_ids)
                triage.assert_called_with(dw_obj, issueregex_ids)
                report_issues.assert_called_with(triage.return_value.matches[:1])
                if exp_create:
                    rsps.assert_call_count(triaged.url, 1)
                else:
                    rsps.assert_call_count(triaged.url, 0)
