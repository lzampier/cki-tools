"""Test parser.py"""
import contextlib
import io
import json
from pathlib import Path
import tempfile
import unittest
from unittest import mock

from cki_lib.kcidb import MAJOR_VERSION
from cki_lib.kcidb import MINOR_VERSION
from cki_lib.kcidb.file import KCIDBFile
from cki_lib.kcidb.file import ObjectNotFound
from freezegun import freeze_time
from jsonschema.exceptions import ValidationError

from cki.kcidb.parser import main


class TestKCIDBParser(unittest.TestCase):
    """Test main function from cki.kcidb.parser."""

    # Copied from cki_lib
    checkout1 = {
        'id': 'redhat:checkout1',
        'origin': 'redhat'
    }
    checkout2 = {
        'id': 'redhat:checkout2',
        'origin': 'redhat'
    }

    # Copied from cki_lib
    build1 = {
        'id': 'redhat:build1',
        'checkout_id': 'redhat:checkout1',
        'origin': 'redhat'
    }
    build2 = {
        'id': 'redhat:build2',
        'checkout_id': 'redhat:checkout2',
        'origin': 'redhat',
        'output_files': [{'name': 'foo', 'url': 'https://foo'}]
    }
    test1 = {
        'id': 'redhat:test1',
        'build_id': 'redhat:build1',
        'origin': 'redhat'
    }
    test2 = {
        'id': 'redhat:test2',
        'build_id': 'redhat:build1',
        'origin': 'redhat'
    }

    # Copied from cki_lib
    valid_content = {
        'version': {'major': MAJOR_VERSION, 'minor': MINOR_VERSION},
        'tests': [],
    }

    # Copied from cki_lib
    @classmethod
    @contextlib.contextmanager
    def dump_kcidb_json(cls, kcidb_data):
        tmpdir = tempfile.TemporaryDirectory()
        path = Path(tmpdir.name, 'kcidb_data.json')
        path.write_text(json.dumps(kcidb_data))
        try:
            yield path
        finally:
            tmpdir.cleanup()

    # Copied from cki_lib
    @classmethod
    @contextlib.contextmanager
    def create_kcidb_file(cls, kcidb_data):
        with cls.dump_kcidb_json(kcidb_data) as path:
            kcidb_file = KCIDBFile(str(path))
            try:
                yield (path, kcidb_file)
            finally:
                pass

    def test_set(self):
        """Test set."""
        kcidb_data = {**self.valid_content, 'builds': [self.build1]}
        for command, key, value in (
            ('set', 'id', 'redhat:build2'),
            ('set-bool', 'valid', True),
            ('set-int', 'duration', 0),
        ):
            with self.subTest(command=command):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    main([str(kcidb_file_path), 'build', 'redhat:build1', command, key, str(value)])
                    kcidb_file2 = KCIDBFile(str(kcidb_file_path))
                    self.assertEqual(value, kcidb_file2.build[key])

    def test_set_nested(self):
        """Test set of nested keys."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat'}
            ]
        }
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'checkout', 'redhat:1234',
                  'set-bool', 'misc/scratch', 'true'])
            kcidb_file2 = KCIDBFile(str(kcidb_file_path))
            self.assertEqual(True, kcidb_file2.checkout['misc']['scratch'])

    def test_set_raises_type(self):
        """Test set-bool and set-int raise if the value passed is not the correct type."""
        kcidb_data = {**self.valid_content, 'builds': [self.build1]}
        for command, key, value in (
            ('set-bool', 'valid', 'Tru'),
            ('set-int', 'duration', '0..'),
        ):
            with self.subTest(command=command), \
                    self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file), \
                    self.assertRaises(SystemExit):
                main([str(kcidb_file_path), 'build', 'redhat:build1', command, key, value])

    def test_set_raises_partial(self):
        """Test set raises if a valid attribute is set but the file is still not valid."""
        for attr in ('build', 'checkout'):
            obj_id = f'redhat:{attr}2'
            with self.subTest(attr=attr), \
                    self.create_kcidb_file(self.valid_content) as (kcidb_file_path, kcidb_file), \
                    self.assertRaises(ObjectNotFound):
                main([str(kcidb_file_path), attr, obj_id, 'set', 'id', obj_id])

    @freeze_time('2021-01-01T00:00:00.0+00:00')
    def test_set_time_now(self):
        """Test set time with 'now' value."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat'}
            ]
        }
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'checkout', 'redhat:1234',
                  'set-time', 'start_time', 'now'])
            kcidb_file2 = KCIDBFile(str(kcidb_file_path))
            self.assertEqual('2021-01-01T00:00:00+00:00', kcidb_file2.checkout['start_time'])

    def test_get(self):
        """Test get."""
        for attr, kcidb_data in (
            ('build', {**self.valid_content, 'builds': [self.build1]}),
            ('checkout', {**self.valid_content, 'checkouts': [self.checkout1]}),
            ('test', {**self.valid_content, 'tests': [self.test1]}),
        ):
            with self.subTest(attr=attr):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                        main([str(kcidb_file_path), attr, f'redhat:{attr}1', 'get', 'id'])
                        self.assertEqual(mock_stdout.getvalue(),
                                         f'{kcidb_data[f"{attr}s"][0]["id"]}\n')

    def test_get_nested(self):
        """Test get nested keys."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat', 'misc': {'scratch': True}}
            ]
        }
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                main([str(kcidb_file_path), 'checkout', 'redhat:1234', 'get', 'misc/scratch'])
                self.assertEqual(mock_stdout.getvalue(), 'True\n')

    def test_get_json(self):
        """Test get json encoded."""
        kcidb_data = {**self.valid_content, 'builds': [self.build2]}
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                main([str(kcidb_file_path), 'build', 'redhat:build2', 'get',
                      'output_files', '--json'])
                self.assertEqual(
                    '[{"name": "foo", "url": "https://foo"}]\n',
                    mock_stdout.getvalue())

    def test_create(self):
        for attr, obj_id, args in (
            ('checkout', self.checkout1['id'], []),
            ('build', self.build1['id'], [self.build1['checkout_id']]),
            ('test', self.test1['id'], [self.test1['build_id']]),
        ):
            with tempfile.TemporaryDirectory() as tmpdir:
                kcidb_file_path = Path(tmpdir, 'kcidb_data.json')
                main([str(kcidb_file_path), attr, obj_id, 'create'] + args)
                self.assertEqual(
                    getattr(KCIDBFile(str(kcidb_file_path)), f'get_{attr}')(obj_id),
                    getattr(self, f'{attr}1'),
                )

    def test_create_raises(self):
        for attr, obj_id, args in (
            ('checkout', 'INVALID_ID', []),
            ('build', 'INVALID_ID', ['INVALID_CHECKOUT_ID']),
        ):
            with tempfile.TemporaryDirectory() as tmpdir:
                kcidb_file_path = Path(tmpdir, 'kcidb_data.json')
                with self.assertRaises(ValidationError):
                    main([str(kcidb_file_path), attr, obj_id, 'create'] + args)

    def test_get_multiple(self):
        """Test get call when there are multiple objects in the file."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [self.checkout1, self.checkout2],
            'builds': [self.build1, self.build2],
        }

        cases = [
            ('checkout', 'redhat:checkout1'),
            ('checkout', 'redhat:checkout2'),
            ('build', 'redhat:build1'),
            ('build', 'redhat:build2'),
        ]

        for attr, obj_id in cases:
            with self.subTest(attr=attr, obj_id=obj_id):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                        main([str(kcidb_file_path), attr, obj_id, 'get', 'id'])
                        self.assertEqual(mock_stdout.getvalue(), f'{obj_id}\n')

    def test_set_multiple(self):
        """Test set call when there are multiple objects in the file."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [self.checkout1, self.checkout2],
            'builds': [self.build1, self.build2],
            'tests': [self.test1, self.test2],
        }

        for attr in ['checkout', 'build', 'test']:
            with (self.subTest(attr=attr),
                  self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file)):
                main([str(kcidb_file_path), attr, f'redhat:{attr}1',
                      'set-bool', 'misc/retrigger', 'False'])
                # reload file
                kcidb_file = KCIDBFile(kcidb_file_path)
                getter = getattr(kcidb_file, f'get_{attr}')
                self.assertEqual(getter(f'redhat:{attr}1')['misc']['retrigger'], False)

                # Make sure it didn't touch redhat:{attr}2
                self.assertEqual(
                    getter(f'redhat:{attr}2'), getattr(self, f'{attr}2')
                )

    def test_create_multiple(self):
        """Test create call multiple times."""
        with self.create_kcidb_file(self.valid_content) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'checkout', 'redhat:1', 'create'])
            main([str(kcidb_file_path), 'checkout', 'redhat:2', 'create'])
            main([str(kcidb_file_path), 'build', 'redhat:2', 'create', 'redhat:1'])
            main([str(kcidb_file_path), 'build', 'redhat:3', 'create', 'redhat:1'])
            main([str(kcidb_file_path), 'test', 'redhat:4', 'create', 'redhat:2'])

            self.assertEqual(
                KCIDBFile(str(kcidb_file_path)).data,
                {
                    'version': {'major': MAJOR_VERSION, 'minor': MINOR_VERSION},
                    'checkouts': [
                        {'id': 'redhat:1', 'origin': 'redhat'},
                        {'id': 'redhat:2', 'origin': 'redhat'},
                    ],
                    'builds': [
                        {'id': 'redhat:2', 'checkout_id': 'redhat:1', 'origin': 'redhat'},
                        {'id': 'redhat:3', 'checkout_id': 'redhat:1', 'origin': 'redhat'},
                    ],
                    'tests': [
                        {'id': 'redhat:4', 'build_id': 'redhat:2', 'origin': 'redhat'},
                    ],
                }
            )

    def test_add_append_dict(self):
        """Test append_dict."""
        kcidb_data = {**self.valid_content, 'builds': [self.build1]}
        name = 'coverage result'
        url = 'http://localhost?name1=value1&name2=value2'
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'build', 'redhat:build1', 'append-dict',
                  'output_files', f'name={name}', f'url={url}'])
            kcidb_file2 = KCIDBFile(str(kcidb_file_path))
            self.assertEqual(True, isinstance(kcidb_file2.build['output_files'], list))
            self.assertEqual(1, len(kcidb_file2.build['output_files']))
            self.assertEqual({'name': name, 'url': url},
                             kcidb_file2.build['output_files'][0])

    def test_add_append_json(self):
        """Test append_json."""
        kcidb_data = {**self.valid_content, 'builds': [self.build1]}
        data = '{"name": "coverage result", "url": "http://url"}'
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'build', 'redhat:build1', 'append-json',
                  'output_files', data])
            kcidb_file = KCIDBFile(str(kcidb_file_path))
            self.assertEqual(True, isinstance(kcidb_file.build['output_files'], list))
            self.assertEqual(1, len(kcidb_file.build['output_files']))
            self.assertEqual({'name': 'coverage result', 'url': 'http://url'},
                             kcidb_file.build['output_files'][0])

    def test_set_json(self):
        """Test set_json."""
        kcidb_data = {**self.valid_content, 'checkouts': [self.checkout1]}
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'checkout', 'redhat:checkout1', 'set-json',
                  'contacts', '["contact1", "contact2"]'])
            kcidb_file = KCIDBFile(str(kcidb_file_path))
            self.assertEqual(True, isinstance(kcidb_file.checkout['contacts'], list))
            self.assertEqual(2, len(kcidb_file.checkout['contacts']))
            self.assertEqual(['contact1', 'contact2'], kcidb_file.checkout['contacts'])
