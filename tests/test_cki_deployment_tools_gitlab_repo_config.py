"""Test cki.deployment_tools.gitlab_repo_config."""
import json
import unittest

import responses

from cki.deployment_tools import gitlab_repo_config


class TestGitLabRepoConfig(unittest.TestCase):
    """Test gitlab_repo_config."""

    @responses.activate
    def test_check_protectedbranches(self) -> None:
        """Test check_protectedbranches."""
        repo_config = {'repo': {
            'branches': None,
            'group': 'group',
            'ignore': False,
            'project': {},
            'url': 'https://host',
            'protectedbranches': {
                '*': {
                    'merge_access_levels': [{'access_level': 30}],
                    'push_access_levels': [{'access_level': 0}],
                    'allow_force_push': False,
                },
            }
        }}
        cases = (
            ('default', [{
                'name': '*',
                'merge_access_levels': [{'access_level': 30}],
                'push_access_levels': [{'access_level': 0}],
                'allow_force_push': False,
            }], None, 0, 0),
            ('missing', [], {
                'name': '*',
                'allow_force_push': False,
                'merge_access_level': 30,
                'push_access_level': 0,
            }, 0, 0),
            ('superfluous', [{
                'name': '*',
                'merge_access_levels': [{'access_level': 30}],
                'push_access_levels': [{'access_level': 0}],
                'allow_force_push': False,
            }, {
                'name': 'other',
                'merge_access_levels': [{'access_level': 0}],
                'push_access_levels': [{'access_level': 0}],
                'allow_force_push': False,
            }], None, 0, 1),
            ('different', [{
                'name': '*',
                'merge_access_levels': [{'access_level': 0}],
                'push_access_levels': [{'access_level': 0}],
                'allow_force_push': False,
            }], {
                'name': '*',
                'allow_force_push': False,
                'merge_access_level': 30,
                'push_access_level': 0,
            }, 1, 0),
        )
        for description, branches, expected_post, expected_delete, expected_delete_other in cases:
            with (self.subTest(description),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://host/api/v4/projects/group%2Frepo', json={
                    'default_branch': 'main',
                    'environments_access_level': 'disabled',
                    'container_registry_enabled': False,
                    'id': 1,
                    'namespace': {'full_path': 'group'},
                    'repository_access_level': 'enabled',
                })
                rsps.get('https://host/api/v4/groups/group', json={})
                rsps.get('https://host/api/v4/projects/1/repository/branches', json=[])
                rsps.get('https://host/api/v4/projects/1/forks?owned=True', json=[])
                rsps.get('https://host/api/v4/projects/1/approvals', json={})
                rsps.get('https://host/api/v4/projects/1/approval_rules', json=[])
                rsps.get('https://host/api/v4/projects/1/push_rule', json={})
                rsps.get('https://host/api/v4/projects/1/protected_branches', json=branches)
                post_request = rsps.post(
                    'https://host/api/v4/projects/1/protected_branches', json={})
                delete_request = rsps.delete(
                    'https://host/api/v4/projects/1/protected_branches/%2A')
                delete_request_other = rsps.delete(
                    'https://host/api/v4/projects/1/protected_branches/other')

                gitlab_repo_config.main(['--config', json.dumps(repo_config), '--fix'])

                if expected_post:
                    self.assertEqual(post_request.call_count, 1)
                    self.assertEqual(json.loads(post_request.calls[0].request.body), expected_post)
                else:
                    self.assertEqual(post_request.call_count, 0)
                self.assertEqual(delete_request.call_count, expected_delete)
                self.assertEqual(delete_request_other.call_count, expected_delete_other)

    @responses.activate
    def test_check_protectedtags(self) -> None:
        """Test check_protectedtags."""
        repo_config = {'repo': {
            'branches': None,
            'group': 'group',
            'ignore': False,
            'project': {},
            'url': 'https://host',
            'protectedtags': {
                '*': {'create_access_levels': [{'access_level': 30}]},
            }
        }}
        cases = (
            ('default', [
                {'name': '*', 'create_access_levels': [{'access_level': 30}]},
            ], None, 0, 0),
            ('missing', [], {'name': '*', 'create_access_level': 30}, 0, 0),
            ('superfluous', [
                {'name': '*', 'create_access_levels': [{'access_level': 30}]},
                {'name': 'other', 'create_access_levels': [{'access_level': 0}]},
            ], None, 0, 1),
            ('different', [{
                'name': '*', 'create_access_levels': [{'access_level': 0}],
            }], {'name': '*', 'create_access_level': 30}, 1, 0),
        )
        for description, tags, expected_post, expected_delete, expected_delete_other in cases:
            with (self.subTest(description),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://host/api/v4/projects/group%2Frepo', json={
                    'default_branch': 'main',
                    'environments_access_level': 'disabled',
                    'container_registry_enabled': False,
                    'id': 1,
                    'namespace': {'full_path': 'group'},
                    'repository_access_level': 'enabled',
                })
                rsps.get('https://host/api/v4/groups/group', json={})
                rsps.get('https://host/api/v4/projects/1/repository/branches', json=[])
                rsps.get('https://host/api/v4/projects/1/forks?owned=True', json=[])
                rsps.get('https://host/api/v4/projects/1/approvals', json={})
                rsps.get('https://host/api/v4/projects/1/approval_rules', json=[])
                rsps.get('https://host/api/v4/projects/1/push_rule', json={})
                rsps.get('https://host/api/v4/projects/1/protected_tags', json=tags)
                post_request = rsps.post(
                    'https://host/api/v4/projects/1/protected_tags', json={})
                delete_request = rsps.delete(
                    'https://host/api/v4/projects/1/protected_tags/%2A')
                delete_request_other = rsps.delete(
                    'https://host/api/v4/projects/1/protected_tags/other')

                gitlab_repo_config.main(['--config', json.dumps(repo_config), '--fix'])

                if expected_post:
                    self.assertEqual(post_request.call_count, 1)
                    self.assertEqual(json.loads(post_request.calls[0].request.body), expected_post)
                else:
                    self.assertEqual(post_request.call_count, 0)
                self.assertEqual(delete_request.call_count, expected_delete)
                self.assertEqual(delete_request_other.call_count, expected_delete_other)
