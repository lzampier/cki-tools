"""Test main.py."""
import unittest
from unittest import mock

from cki_tools.pipeline_herder import main


class TestNotifyFinished(unittest.TestCase):
    """Test notify_finished."""

    @staticmethod
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch.multiple('cki_tools.pipeline_herder.settings',
                         RABBITMQ_PUBLISH_EXCHANGE='exchange')
    @mock.patch('cki_lib.messagequeue.MessageQueue')
    def test_call(messagequeue):
        """Check send_message is called correctly."""
        main.notify_finished('https://host/cki-project/cki-pipeline/-/jobs/123')
        messagequeue().send_message.assert_called_with(
            {'web_url': 'https://host/cki-project/cki-pipeline/-/jobs/123'},
            'herder.build',
            exchange='exchange',
            headers={'message-type': 'herder', 'message-herder-type': 'build'},
        )

    @staticmethod
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    @mock.patch('cki_lib.messagequeue.MessageQueue')
    def test_call_no_rabbitmq(messagequeue):
        """Check send_message is not called with is_production=False."""
        main.notify_finished('https://host/cki-project/cki-pipeline/-/jobs/123')
        messagequeue().send_message.assert_not_called()
