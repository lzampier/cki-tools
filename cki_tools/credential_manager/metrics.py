"""Credential metrics."""
from datetime import timezone

import dateutil.parser
import prometheus_client

from cki.deployment_tools import secrets

from . import utils


def process() -> str:
    """Return token metrics."""
    registry = prometheus_client.CollectorRegistry()
    metric_token_expires_at = prometheus_client.Gauge(
        'cki_token_expires_at', 'timestamp when validity of token ends', ['name'],
        registry=registry)
    for token in utils.all_tokens({'bugzilla_token', 'gitlab_token'}):
        if expires_at := secrets.secret(f'{token}#').get('expires_at'):
            timestamp = dateutil.parser.parse(expires_at).replace(tzinfo=timezone.utc).timestamp()
            metric_token_expires_at.labels(token).set(timestamp)
    return prometheus_client.generate_latest(registry).decode('utf8')
