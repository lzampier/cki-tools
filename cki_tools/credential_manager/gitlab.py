"""GitLab credential management."""
from datetime import datetime
from datetime import timedelta
from datetime import timezone
import typing

from cki_lib import logger
from cki_lib.gitlab import get_instance
from cki_lib.gitlab import parse_gitlab_url

from cki.deployment_tools import secrets

from . import utils

LOGGER = logger.get_logger(__name__)


def create(token: str) -> None:
    """Create a GitLab token."""
    LOGGER.info('Processing %s', token)
    meta = secrets.secret(f'{token}#')
    match meta['token_type'], meta['gitlab_token_type']:
        case 'gitlab_token', 'project_token':
            gl_instance, gl_owner = parse_gitlab_url(meta['project_url'])
        case 'gitlab_token', 'group_token':
            gl_instance, gl_owner = parse_gitlab_url(meta['group_url'])
        case token_type, gitlab_token_type:
            raise Exception(f'Unable to create {token_type}/{gitlab_token_type} {token}')
    gl_personal_token = gl_owner.access_tokens.create({
        'name': meta['token_name'],
        'expires_at': (datetime.now(timezone.utc) + timedelta(days=365)).date().isoformat(),
        'scopes': meta['scopes'],
        'access_level': meta['access_level'],
    })
    gl_user = gl_instance.users.get(gl_personal_token.user_id)
    meta.update({
        'token_id': gl_personal_token.id,
        'created_at': gl_personal_token.created_at,
        'expires_at': gl_personal_token.expires_at,
        'revoked': gl_personal_token.revoked,
        'active': gl_personal_token.active,
        'user_id': gl_personal_token.user_id,
        'user_name': gl_user.username,
    })
    secrets.edit(f'{token}#', meta)
    secrets.edit(f'{token}', gl_personal_token.token)


def _update_project_token(meta: typing.Dict[str, str], secret_token: str) -> None:
    gl_instance, gl_project = parse_gitlab_url(meta['project_url'])
    gl_project_token = get_instance(gl_instance.url,
                                    token=secret_token).personal_access_tokens.get('self')
    gl_member = gl_project.members.get(gl_project_token.user_id)
    meta.update({
        'scopes': gl_project_token.scopes,
        'access_level': gl_member.access_level,
        'token_name': gl_project_token.name,
        'token_id': gl_project_token.id,
        'created_at': gl_project_token.created_at,
        'expires_at': gl_project_token.expires_at,
        'revoked': gl_project_token.revoked,
        'active': gl_project_token.active,
        'user_id': gl_project_token.user_id,
        'user_name': gl_member.username,
    })


def _update_group_token(meta: typing.Dict[str, str], secret_token: str) -> None:
    gl_instance, gl_group = parse_gitlab_url(meta['group_url'])
    gl_group_token = get_instance(gl_instance.url,
                                  token=secret_token).personal_access_tokens.get('self')
    gl_member = gl_group.members.get(gl_group_token.user_id)
    meta.update({
        'scopes': gl_group_token.scopes,
        'access_level': gl_member.access_level,
        'token_name': gl_group_token.name,
        'token_id': gl_group_token.id,
        'created_at': gl_group_token.created_at,
        'expires_at': gl_group_token.expires_at,
        'revoked': gl_group_token.revoked,
        'active': gl_group_token.active,
        'user_id': gl_group_token.user_id,
        'user_name': gl_member.username,
    })


def _update_personal_token(meta: typing.Dict[str, str], secret_token: str) -> None:
    gl_instance = get_instance(meta['instance_url'])
    gl_personal_token = get_instance(gl_instance.url,
                                     token=secret_token).personal_access_tokens.get('self')
    gl_user = gl_instance.users.get(gl_personal_token.user_id)
    meta.update({
        'scopes': gl_personal_token.scopes,
        'token_name': gl_personal_token.name,
        'token_id': gl_personal_token.id,
        'created_at': gl_personal_token.created_at,
        'expires_at': gl_personal_token.expires_at,
        'revoked': gl_personal_token.revoked,
        'active': gl_personal_token.active,
        'user_id': gl_personal_token.user_id,
        'user_name': gl_user.username,
    })


def _update_runner_token(meta: typing.Dict[str, str], secret_token: str) -> None:
    gl_instance = get_instance(meta['instance_url'])
    response = gl_instance.http_post(
        '/runners/verify', post_data={"token": secret_token})
    meta.update({
        'token_id': response['id'],
    })
    if expires_at := response['token_expires_at']:
        meta['expires_at'] = expires_at


def update(token: str) -> None:
    """Update the secret meta information about GitLab tokens."""
    LOGGER.info('Processing %s', token)
    meta = secrets.secret(f'{token}#')
    token_type = meta['gitlab_token_type']
    LOGGER.debug('Processing %s - token type: %s', token, token_type)
    match token_type:
        case 'project_token':
            _update_project_token(meta,  secrets.secret(token))
        case 'group_token':
            _update_group_token(meta, secrets.secret(token))
        case 'personal_token':
            _update_personal_token(meta, secrets.secret(token))
        case 'runner_authentication_token':
            _update_runner_token(meta, secrets.secret(token))
        case _:
            LOGGER.info('Token meta update for %s %s not supported', token_type, token)
    secrets.edit(f'{token}#', meta)


def validate(token: str) -> None:
    """Check validity of the token."""
    LOGGER.info('Processing %s', token)
    token_secret = secrets.secret(token)
    meta = secrets.secret(f'{token}#')
    token_type = meta['gitlab_token_type']
    LOGGER.debug('Processing %s - token type: %s', token, token_type)
    match token_type:
        case 'project_token':
            gl_instance = get_instance(meta['project_url'], token=token_secret)
            gl_project_token = gl_instance.personal_access_tokens.get('self')
            if gl_project_token.revoked or not gl_project_token.active:
                raise Exception(f'Revoked token {token}')
        case 'group_token':
            gl_instance = get_instance(meta['group_url'], token=token_secret)
            gl_group_token = gl_instance.personal_access_tokens.get('self')
            if gl_group_token.revoked or not gl_group_token.active:
                raise Exception(f'Revoked token {token}')
        case 'personal_token':
            gl_instance = get_instance(meta['instance_url'], token=token_secret)
            gl_personal_token = gl_instance.personal_access_tokens.get('self')
            if gl_personal_token.revoked or not gl_personal_token.active:
                raise Exception(f'Revoked token {token}')
        case 'runner_authentication_token':
            gl_instance = get_instance(meta['instance_url'])
            gl_instance.http_post('/runners/verify', post_data={"token": token_secret})
        case _:
            LOGGER.info('Token validation for %s %s not supported', token_type, token)


def process(action: str, token: str) -> None:
    """Process gitlab actions."""
    match action:
        case 'create':
            create(token)
        case 'update':
            for all_token in utils.all_tokens(['gitlab_token']):
                update(all_token)
        case 'validate':
            for all_token in utils.all_tokens(['gitlab_token']):
                validate(all_token)
