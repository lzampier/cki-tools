"""Various utility methods."""
import os
import typing

from cki_lib import misc
from cki_lib import yaml


def all_tokens(token_types: typing.Container[str]) -> typing.List[str]:
    """Return names of secrets for given token types."""
    return [
        k for k, v in yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')).items()
        if misc.get_nested_key(v, '.meta/token_type') in token_types
    ]
