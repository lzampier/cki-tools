"""Regexes to look for bugs 🐛🐛."""
from contextlib import contextmanager
import dataclasses
from enum import Enum
from functools import lru_cache
import time
import typing

from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.session import get_session
from cki_lib.timeout import func_timeout
from datawarehouse import objects
import prometheus_client as prometheus

from . import cache
from . import compiledregex
from . import dwobject
from . import settings

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)

METRIC_REGEX_SEARCH_TIME = prometheus.Histogram(
    'regex_search_time_seconds',
    'Time spent looking through a log file with the regexes'
)
METRIC_REGEX_MATCH_TIME = prometheus.Summary(
    'regex_match_time_seconds',
    'Time spent matching, grouped by regex.',
    ['regex_id']
)

UNSUCCESSFUL_STATUSES = ['ERROR', 'FAIL']

TriageStatus = Enum('TriageStatus', ['INCOMPLETE', 'NOT_NEEDED', 'SUCCESS'])
MatchStatus = Enum('MatchStatus', ['NOT_APPLICABLE', 'NO_MATCH', 'PARTIAL_MATCH', 'FULL_MATCH'])


@dataclasses.dataclass
class LogFile:
    """One log file belonging to a KCIDB object."""

    dw_obj: objects.RESTObject
    name: str = ''
    url: str = ''


@dataclasses.dataclass
class RegexMatch:
    """A regular expression match."""

    status: MatchStatus
    log_file: LogFile
    regex: compiledregex.CompiledIssueRegex


@dataclasses.dataclass
class TriageResult:
    """Result of triaging an KCIDB object."""

    status: TriageStatus
    matches: list[RegexMatch]


@METRIC_REGEX_SEARCH_TIME.time()
def triage(dw_obj: typing.Any, issueregex_ids: list[int]) -> TriageResult:
    """Use regexes to find failures."""
    # incomplete object
    if (dw_obj.status if dw_obj.type == 'test' else dw_obj.valid) is None:
        return TriageResult(TriageStatus.INCOMPLETE, [])

    # no triage needed
    if dw_obj.type in {'checkout', 'build'} and dw_obj.valid is True:
        return TriageResult(TriageStatus.NOT_NEEDED, [])
    if dw_obj.type == 'test' and dw_obj.status not in UNSUCCESSFUL_STATUSES and not any(
            r.get('status') in UNSUCCESSFUL_STATUSES
            for r in (dw_obj.misc or {}).get('results', [])):
        return TriageResult(TriageStatus.NOT_NEEDED, [])

    # determine log files
    log_files = []
    if dw_obj.type == 'checkout':
        log_files += _log_files(dw_obj, 'merge.log')
    elif dw_obj.type == 'build':
        log_files += _log_files(dw_obj, 'build.log')
    elif dw_obj.type == 'test':
        log_files += _log_files(dw_obj, 'test.log')
        log_files += misc.flattened([
            _log_files(dw_obj, 'test.log', dw_obj_child=dwobject.from_attrs('testresult', attrs={
                **result, 'misc': {**result.get('misc', {}), 'test_name': dw_obj.comment}
            }))
            for result in (dw_obj.misc or {}).get('results', [])
            if result.get('status') in UNSUCCESSFUL_STATUSES
        ])
    LOGGER.debug('There are %d log files to check', len(log_files))

    # no reason to look at regexes if there are no log files to triage
    if not log_files:
        return TriageResult(TriageStatus.SUCCESS, [])

    # really triage
    regexes = compiledregex.get_compiled_issueregexes(issueregex_ids)
    return TriageResult(TriageStatus.SUCCESS, [
        RegexMatch(status, log_file, regex)
        for log_file in log_files
        for regex in regexes
        if (status := match(log_file, regex)) != MatchStatus.NOT_APPLICABLE
    ])


def _match_build(
    build: objects.KCIDBBuild,
    regex: compiledregex.CompiledIssueRegex,
) -> bool:
    architecture_match = regex.architecture_match
    kpet_tree_name_match = regex.kpet_tree_name_match
    package_name_match = regex.package_name_match

    if architecture_match and not (
            build and
            build.architecture and
            architecture_match.search(build.architecture)):
        LOGGER.debug(' architecture "%s" did not match: %s',
                     build.architecture if build else None,
                     architecture_match)
        return False

    if kpet_tree_name_match and not (
            build and
            build.misc.get('kpet_tree_name') and
            kpet_tree_name_match.search(build.misc['kpet_tree_name'])):
        LOGGER.debug(' kpet tree name "%s" did not match: %s',
                     build.misc.get('kpet_tree_name') if build else None,
                     kpet_tree_name_match)
        return False

    if package_name_match and not (
            build and
            build.misc.get('package_name') and
            package_name_match.fullmatch(build.misc['package_name'])):
        LOGGER.debug(' package name "%s" did not match: %s',
                     build.misc.get('package_name') if build else None,
                     package_name_match)
        return False
    return True


def _match_checkout(
    checkout: objects.KCIDBCheckout,
    regex: compiledregex.CompiledIssueRegex,
) -> bool:
    tree_match = regex.tree_match

    if tree_match and not (
            checkout and
            checkout.tree_name and
            tree_match.search(checkout.tree_name)):
        LOGGER.debug(' tree name "%s" did not match: %s',
                     checkout.tree_name if checkout else None,
                     tree_match)
        return False
    return True


def match(
    log_file: LogFile,
    regex: compiledregex.CompiledIssueRegex,
) -> MatchStatus:
    # pylint: disable=too-many-return-statements,too-many-branches
    """Match regex against log."""
    text_match = regex.text_match
    test_name_match = regex.test_name_match
    testresult_name_match = regex.testresult_name_match
    file_name_match = regex.file_name_match

    if file_name_match and not (log_file.name and file_name_match.search(log_file.name)):
        LOGGER.debug(' file name "%s" did not match: %s', log_file.name, file_name_match)
        return MatchStatus.NOT_APPLICABLE

    if testresult_name_match and testresult_name_match.pattern != r'.*':
        if log_file.dw_obj.type == 'testresult':
            if not (log_file.dw_obj.comment and
                    testresult_name_match.search(log_file.dw_obj.comment)):
                LOGGER.debug(' testresult name "%s" did not match: %s',
                             log_file.dw_obj.comment, testresult_name_match)
                return MatchStatus.NOT_APPLICABLE
        else:
            LOGGER.debug(" obj.type=%r can't be matched if testresult_name_match != '.*' (%r)",
                         log_file.dw_obj.type, testresult_name_match)
            return MatchStatus.NOT_APPLICABLE

    if test_name_match:
        if log_file.dw_obj.type == 'testresult':
            test_name = log_file.dw_obj.misc.get("test_name")
        elif log_file.dw_obj.type == 'test':
            test_name = log_file.dw_obj.comment
        else:
            LOGGER.debug(' obj type %r did not match "test" nor "testresult"', log_file.dw_obj.type)
            return MatchStatus.NOT_APPLICABLE

        if not (test_name and test_name_match.search(test_name)):
            LOGGER.debug(' test name "%s" did not match: %s', test_name, test_name_match)
            return MatchStatus.NOT_APPLICABLE

    if text_match:
        if not log_file.url:
            LOGGER.error(" missing URL in file (%r) therefore didn't match: %s",
                         log_file, text_match)
            return MatchStatus.NO_MATCH

        if (text := download(log_file.url)) is None:
            LOGGER.debug(" file text wasn't retrieved therefore didn't match: %s",
                         text_match)
            return MatchStatus.NO_MATCH

        with _match_time_measure(regex):
            timeout_secs = settings.REGEX_TIMEOUT.total_seconds()
            if not func_timeout(text_match.search, timeout_secs, args=[text]):
                LOGGER.debug(' file text (len=%d) did not match: %s',
                             len(text), text_match)
                return MatchStatus.NO_MATCH

    # We got this far, do the expensive queries.
    if log_file.dw_obj.type == "checkout":
        build = None
        checkout = log_file.dw_obj
    elif log_file.dw_obj.type == "build":
        build = log_file.dw_obj
        checkout = cache.get_checkout(cache.get_cache_ttl(), build.checkout_id)
    else:  # test|testresult
        build = cache.get_build(cache.get_cache_ttl(), log_file.dw_obj.build_id)
        checkout = cache.get_checkout(cache.get_cache_ttl(), build.checkout_id)

    if not _match_build(build, regex):
        LOGGER.debug(' associated build properties did not match')
        return MatchStatus.PARTIAL_MATCH

    if not _match_checkout(checkout, regex):
        LOGGER.debug(' associated checkout properties did not match')
        return MatchStatus.PARTIAL_MATCH

    LOGGER.info('The obj=%r successfully matched the regex %r', log_file.dw_obj, regex)

    return MatchStatus.FULL_MATCH


@contextmanager
def _match_time_measure(
    regex: compiledregex.CompiledIssueRegex,
) -> typing.Generator[None, None, None]:
    """Measure the time matching a regex."""
    start = time.time()
    try:
        yield
    finally:
        elapsed = time.time() - start
        METRIC_REGEX_MATCH_TIME.labels(regex_id=regex.id).observe(elapsed)
        if elapsed > settings.REGEX_EXPECTED_TIME.total_seconds():
            LOGGER.error("Regex matching took too long. regex_id=%d elapsed_s=%f",
                         regex.id, elapsed)


def _log_files(dw_obj: typing.Any, log_file_name: str,
               dw_obj_child: typing.Any = None) -> list[LogFile]:
    """Return all applicable log files for a KCIDB object."""
    log_files = []
    dw_obj_target = dw_obj_child or dw_obj
    if log_url := getattr(dw_obj, 'log_url', None):
        log_files += [LogFile(dw_obj_target, log_file_name, log_url)]
    if output_files := getattr(dw_obj, 'output_files', []):
        log_files += [LogFile(dw_obj_target, **o) for o in output_files]
    if dw_obj_child and (child_output_files := getattr(dw_obj_child, 'output_files', [])):
        log_files += [LogFile(dw_obj_target, **o) for o in child_output_files]
    return log_files


@lru_cache(maxsize=1)
def download(file_url: str) -> typing.Optional[str]:
    """Fetch the content of the given file if it has ContentType='text/plain'."""
    log_content: typing.Optional[str] = None
    with misc.only_log_exceptions():
        headers = SESSION.head(file_url, allow_redirects=True).headers

        content_type = str(headers.get('content-type'))

        try:
            content_length = int(headers.get('content-length', 0))
        except (TypeError, ValueError):
            content_length = 0

        LOGGER.debug("Reading %r with content_type=%r and content_length=%r",
                     file_url, content_type, content_length)

        # NOTE: Content_type string can contain "charset" and "boundary"
        # ref: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type
        if ('text/plain' not in content_type and content_length > settings.MAX_CONTENT_LENGTH):
            LOGGER.warning("Rejecting to read file %r with content_type=%r and content_length=%r",
                           file_url, content_type, content_length)
            return None

        log_content = SESSION.get(file_url).content.decode(errors='ignore')
    return log_content
