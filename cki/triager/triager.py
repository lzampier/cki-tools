"""Datawarehouse failure triager."""
import argparse
from http import HTTPStatus
import os
import typing

from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from cki_lib.logger import get_logger
import requests.exceptions
import sentry_sdk

from . import checkers
from . import dwobject

LOGGER = get_logger('cki.triager')
IS_PRODUCTION_OR_STAGING = misc.is_production_or_staging()

OBJECT_TYPE_TRIAGED = {'checkout', 'build', 'test'}
STATUS_TRIAGED = {'new', 'updated', 'needs_triage'}


class Triager:
    """Triage and report a KCIDB object."""

    def __init__(self) -> None:
        """Create instance."""
        # {(obj.type, obj.id): tagged_issue_ids}
        self.issue_cache: dict[tuple[str, str], set[int]] = {}

    def needs_linking(self, match: checkers.RegexMatch) -> typing.Optional[tuple[str, str]]:
        """Return the cache key if an object is not already tagged with an issue."""
        if (key := (match.log_file.dw_obj.type, match.log_file.dw_obj.id)) not in self.issue_cache:
            self.issue_cache[key] = {i.id for i in match.log_file.dw_obj.issues.list()}
        return key if match.regex.issue_id not in self.issue_cache[key] else None

    def report_issues(self, matches: list[checkers.RegexMatch]) -> None:
        """Report a list of issues."""
        for match in matches:
            if not (key := self.needs_linking(match)):
                LOGGER.debug('Already linked: obj=%s to issue=%s',
                             match.log_file.dw_obj, match.regex.issue_id)
                continue
            if IS_PRODUCTION_OR_STAGING:
                LOGGER.info('Linking obj=%s to issue=%s',
                            match.log_file.dw_obj, match.regex.issue_id)
                # we could actually report the matching log file and regex id
                # as well, but DW currently has no support for that 😕
                match.log_file.dw_obj.issues.create(issue_id=match.regex.issue_id)
            else:
                LOGGER.info('Would link obj=%s to issue=%s in prod',
                            match.log_file.dw_obj, match.regex.issue_id)
            self.issue_cache[key].add(match.regex.issue_id)

    def check(self, dw_obj: typing.Any, issueregex_ids: list[int]) -> None:
        """Check object for issues."""
        LOGGER.debug('Checking type=%s id=%s', dw_obj.type, dw_obj.id)
        triage_result = checkers.triage(dw_obj, issueregex_ids)

        self.report_issues([m for m in triage_result.matches
                            if m.status == checkers.MatchStatus.FULL_MATCH])

        if triage_result.status == checkers.TriageStatus.INCOMPLETE:
            LOGGER.info('Not marking as triaged because incomplete obj=%s', dw_obj)
        elif issueregex_ids and not triage_result.matches:
            LOGGER.info('Not marking as triaged as individual regex without matches obj=%s', dw_obj)
        elif IS_PRODUCTION_OR_STAGING:
            try:
                # Tag this object as triaged
                dw_obj.action_triaged.create()
            except requests.exceptions.HTTPError as exception:
                # the object might have been deleted in the meanwhile
                if exception.response.status_code == HTTPStatus.NOT_FOUND:
                    LOGGER.exception('Unable to find %r, assuming it was deleted', dw_obj)
                else:
                    raise
        else:
            LOGGER.info('Would mark obj=%s as triaged in prod', dw_obj)


def callback(body: typing.Any = None, **_: typing.Any) -> None:
    """Handle a single message."""
    obj_data = body['object']
    obj_type = body['object_type']
    status = body['status']
    msg_misc = body.get('misc') or {}
    issueregex_ids = msg_misc.get('issueregex_ids') or []
    LOGGER.info('Got message for (%s) %s id=%s misc=%s',
                status, obj_type, obj_data['id'], msg_misc)
    if status in STATUS_TRIAGED and obj_type in OBJECT_TYPE_TRIAGED:
        dw_obj = dwobject.from_attrs(obj_type, attrs=obj_data)
        Triager().check(dw_obj, issueregex_ids)


def main(args: typing.Optional[list[str]] = None) -> None:
    """CLI Interface."""
    misc.sentry_init(sentry_sdk)
    parser = argparse.ArgumentParser()
    parser.add_argument('type', nargs='?', choices=['checkout', 'build', 'test'],
                        help='Kind of stuff to check')
    parser.add_argument('id', nargs='?', help='Id or iid of the object to check')
    parser.add_argument('--regex-id', help='Only check specified issue regex')
    arguments = parser.parse_args(args)

    if arguments.type and arguments.id:
        issueregex_ids = [arguments.regex_id] if arguments.regex_id else []
        dw_obj = dwobject.from_obj_id(arguments.type, obj_id=arguments.id)
        Triager().check(dw_obj, issueregex_ids)
        return

    metrics.prometheus_init()
    LOGGER.info("Running checks on queue items.")
    messagequeue.MessageQueue().consume_messages(
        os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.exchange.webhooks'),
        os.environ['DATAWAREHOUSE_TRIAGER_ROUTING_KEYS'].split(),
        callback,
        queue_name=os.environ.get('DATAWAREHOUSE_TRIAGER_QUEUE'),
        max_priority=1)
