---
title: cki_tools.gating_reporter
linkTitle: gating_reporter
description: Webhook responsible for UMB message sending related to OSCI gating
---

The following messages will be sent:

- `/topic/VirtualTopic.eng.ci.cki.brew-build.test.running`: in response to
  DataWarehouse `build_setups_finished`
- `/topic/VirtualTopic.eng.ci.cki.brew-build.test.complete`: in response to
  DataWarehouse `ready_to_report`

For details about the schema and use case, check out [Fedora CI messages].
**BE SURE TO RUN THE MESSAGE VALIDATION BEFORE CHANGING ANYTHING IN THE MESSAGES!**
The validation script and guidelines are available [on Pagure].

## How to run this project

The service can be deployed locally by running

```bash
python -m cki_tools.gating_reporter
```

## Configuration via environment variables

| Name                                  | Secret | Required | Description                                                                                                |
|---------------------------------------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `DATAWAREHOUSE_URL`                   | no     | yes      | DataWarehouse URL                                                                                          |
| `DATAWAREHOUSE_TOKEN_GATING_REPORTER` | yes    | yes      | DataWarehouse access token                                                                                 |
| `SUPPORTED_KOJI_INSTANCES`            | no     | yes      | JSON/YAML-formatted list of supported koji instances, see below                                            |
| `STOMP_HOST`                          | no     | yes      | UMB host                                                                                                   |
| `STOMP_PORT`                          | no     | yes      | UMB port                                                                                                   |
| `STOMP_CERTFILE`                      | yes    | yes      | UMB certificate                                                                                            |
| `RABBITMQ_HOST`                       | no     | no       | AMQP host                                                                                                  |
| `RABBITMQ_PORT`                       | no     | no       | AMQP port, TLS is used for port 443                                                                        |
| `RABBITMQ_VIRTUAL_HOST`               | no     | no       | AMQP vhost, defaults to /                                                                                  |
| `RABBITMQ_USER`                       | no     | no       | AMQP user                                                                                                  |
| `RABBITMQ_PASSWORD`                   | yes    | no       | AMQP password                                                                                              |
| `RABBITMQ_CAFILE`                     | no     | no       | AMQP CA file path                                                                                          |
| `WEBHOOK_RECEIVER_EXCHANGE`           | no     | no       | AMQP exchange to receive messages                                                                          |
| `GATING_REPORTER_QUEUE`               | no     | no       | AMQP queue name to use on the exchange                                                                     |
| `GATING_REPORTER_ROUTING_KEYS`        | no     | no       | AMQP routing keys for the messages sent to the queue                                                       |
| `CKI_DEPLOYMENT_ENVIRONMENT`          | no     | no       | Define the deployment environment (production/staging)                                                     |
| `CKI_LOGGING_LEVEL`                   | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |
| `CKI_METRICS_ENABLED`                 | no     | no       | Enable prometheus metrics. Default: false                                                                  |
| `CKI_METRICS_PORT`                    | no     | no       | Port where prometheus metrics are exposed. Default: 8000                                                   |

### Configuring supported Koji instances

```yaml
- web_url: https://kojihub.stream.centos.org/koji/
  artifact_type: koji-build
  message_topic: koji-build
```

Messages will only be triggered for supported Koji instances. Checkouts will be
filtered based on the provenance data that needs to match the Koji web URL. The
gating messages will have the correct artifact type and message topic
configured.

## Manual triggering

Messages can also be manually triggered via the command line. First, make
sure to have the required environment variables above configured correctly.
Then, the sending of UMB messages can be triggered with

```bash
CKI_LOGGING_LEVEL=INFO \
  CKI_DEPLOYMENT_ENVIRONMENT=production \
  python3 -m cki_tools.gating_reporter \
  --message-type MESSAGE_TYPE \
  --checkout-id CHECKOUT_ID
```

- `MESSAGE_TYPE` can be `running` or `complete`
- `CHECKOUT_ID` is the KCIDB checkout ID.

[Fedora CI messages]: https://pagure.io/fedora-ci/messages/
[on Pagure]: https://pagure.io/fedora-ci/messages
