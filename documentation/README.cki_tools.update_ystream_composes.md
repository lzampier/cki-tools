---
title: cki_tools.update_ystream_composes
linkTitle: update_ystream_composes
description: Update the y-stream composes for trees in kpet-db
---

## Usage

```shell
usage: python3 -m cki_tools.update_ystream_composes [-h]
    [--config CONFIG] [--config-path CONFIG_PATH]

Output the patch that would be needed to update y-stream trees in kpet-db

options:
  -h, --help            show this help message and exit
  --config CONFIG       YAML y-stream composes configuration file to use
  --config-path CONFIG_PATH
                        Path to YAML y-stream composes configuration file
```

## Configuration via environment variables

The following variables need to be defined:

| Name                           | Type   | Secret | Required | Description                                                                                                |
|--------------------------------|--------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `YSTREAM_COMPOSES_CONFIG`      | yaml   | no     | no       | Configuration in YAML. If not present, falls back to `YSTREAM_COMPOSES_CONFIG_PATH`                        |
| `YSTREAM_COMPOSES_CONFIG_PATH` | path   | no     | no       | Path to the configuration YAML file                                                                        |
| `CTS_URL`                      | url    | no     | yes      | URL of the compose tracking service                                                                        |
| `CKI_DEPLOYMENT_ENVIRONMENT`   | string | no     | no       | Define the deployment environment (production/staging)                                                     |
| `CKI_LOGGING_LEVEL`            | string | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |
| `SENTRY_DSN`                   | url    | yes    | no       | Sentry DSN                                                                                                 |

```bash-session
$ python3 -m cki_tools.update_ystream_composes --config-path ystream_composes.yaml
--- trees/rhel9.j2
+++ trees/rhel9.j2
@@ -1,5 +1,5 @@
-{% set distro_name = "RHEL-9.4.0-20231008.18" %}
-{% set buildroot_name = "BUILDROOT-9.4.0-RHEL-9-20231008.22" %}
+{% set distro_name = "RHEL-9.4.0-20231016.19" %}
+{% set buildroot_name = "BUILDROOT-9.4.0-RHEL-9-20231016.25" %}
 {% set distro_requires %}
   <variant op="=" value="BaseOS"/>
   <distro_family op="=" value="RedHatEnterpriseLinux9"/>
```
