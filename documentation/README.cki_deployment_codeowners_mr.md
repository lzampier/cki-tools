---
title: cki_deployment_codeowners_mr.sh
description: Create MRs of changed CODEOWNERs configuration
aliases: [/l/gitlab-codeowners-mr-docs]
---

```shell
cki_deployment_codeowners_mr.sh
```

Merge requests for the changes are opened in the GitLab project specified in
`GITLAB_PROJECT_URL`. The changes are pushed to a branch in the project
specified via `GITLAB_FORK_URL`, which can be either a fork or the same
project.

The underlying functionality is implemented in
`cki.deployment_tools.gitlab_codeowners_config`.

The required permissions of the `GITLAB_TOKEN` vary depending on the precise
repository setup. In general, developer permissions are required to be able to
push code to a repository. In public repositories, (implicit) guest permissions
are good enough to open a new merge request. See the GitLab documention on
[permissions and roles] for details.

## Environment variables

| Field                | Type   | Required | Description                                       |
|----------------------|--------|----------|---------------------------------------------------|
| `GITLAB_PROJECT_URL` | string | yes      | Git repo URL without protocol                     |
| `GITLAB_FORK_URL`    | string | yes      | URL of the fork of the Git repo without protocol  |
| `GITLAB_TOKEN`       | string | yes      | GitLab private token with access to the Git repos |
| `BRANCH_NAME`        | string | yes      | Branch name in the fork of the git repo           |
| `SOURCE_BRANCH_NAME` | string | no       | Source branch name to checkout (default `main`)   |
| `REVIEWERS`          | string | yes      | GitLab users to notify in MR description          |
| `GIT_USER_NAME`      | string | yes      | Git user name                                     |
| `GIT_USER_EMAIL`     | string | yes      | Git user email                                    |

[permissions and roles]: https://docs.gitlab.com/ee/user/permissions.html
