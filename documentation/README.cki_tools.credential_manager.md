---
title: cki_tools.credential_manager
linkTitle: credential_manager
description: Manage CKI service account secrets across services
aliases: [/l/credential-manager-docs]
---

Manage credentials for service accounts and their meta data as stored in CKI secrets.

## Metrics

Usage:

```bash
python -m cki_tools.credential_manager metrics
```

This will output the following Prometheus metrics related to the stored credentials:

| name                   | supported `token_type`           | description         |
|------------------------|----------------------------------|---------------------|
| `cki_token_expires_at` | `bugzilla_token`, `gitlab_token` | ISO8601 expiry date |

## GitLab

For various kinds of GitLab tokens, the tool supports

- creation (`create`)

  ```bash
  python -m cki_tools.credential_manager gitlab create --token TOKEN_SECRET_NAME
  ```

- meta data update (`update`)

  ```bash
  python -m cki_tools.credential_manager gitlab update
  ```

- validation (`validate`)

  ```bash
  python -m cki_tools.credential_manager gitlab validate
  ```

### Project access tokens

See the [API description][project-api] for details.

| name                | create   | update   | validate | description                          |
|---------------------|----------|----------|----------|--------------------------------------|
| (secret)            |          | required | required | secret token                         |
| `token_type`        | required | required | required | `gitlab_token`                       |
| `gitlab_token_type` | required | required | required | `project_token`                      |
| `project_url`       | required | required | required | Project URL                          |
| `scopes`            | required | updated  |          | Access scope                         |
| `access_level`      | required | updated  |          | Access levels                        |
| `token_name`        | required | updated  |          | Name of the token                    |
| `token_id`          | updated  | updated  |          | Project token ID                     |
| `created_at`        | updated  | updated  |          | ISO8601 timestamp of creation        |
| `expires_at`        | updated  | updated  |          | ISO8601 expiry date                  |
| `revoked`           | updated  | updated  |          | Whether the token is already revoked |
| `active`            | updated  | updated  |          | Whether the token is still active    |
| `user_id`           | updated  | updated  |          | ID of associated user                |
| `user_name`         | updated  | updated  |          | Name of associated user              |

### Group access tokens

See the [API description][group-api] for details.

| name                | create   | update   | validate | description                          |
|---------------------|----------|----------|----------|--------------------------------------|
| (secret)            |          | required | required | secret token                         |
| `token_type`        | required | required | required | `gitlab_token`                       |
| `gitlab_token_type` | required | required | required | `group_token`                        |
| `group_url`         | required | required | required | Group URL                            |
| `scopes`            | required | updated  |          | Access scope                         |
| `access_level`      | required | updated  |          | Access levels                        |
| `token_name`        | required | updated  |          | Name of the token                    |
| `token_id`          | updated  | updated  |          | Group token ID                       |
| `created_at`        | updated  | updated  |          | ISO8601 timestamp of creation        |
| `expires_at`        | updated  | updated  |          | ISO8601 expiry date                  |
| `revoked`           | updated  | updated  |          | Whether the token is already revoked |
| `active`            | updated  | updated  |          | Whether the token is still active    |
| `user_id`           | updated  | updated  |          | ID of associated user                |
| `user_name`         | updated  | updated  |          | Name of associated user              |

### Personal access tokens

See the [API description][personal-api] for details.

Token creation is not supported.

| name                | update   | validate | description                          |
|---------------------|----------|----------|--------------------------------------|
| (secret)            | required | required | secret token                         |
| `token_type`        | required | required | `gitlab_token`                       |
| `gitlab_token_type` | required | required | `personal_token`                     |
| `instance_url`      | required | required | GitLab instance URL                  |
| `scopes`            | updated  |          | Access scope                         |
| `token_name`        | updated  |          | Name of the token                    |
| `token_id`          | updated  |          | Access token ID                      |
| `created_at`        | updated  |          | ISO8601 timestamp of creation        |
| `expires_at`        | updated  |          | ISO8601 expiry date                  |
| `revoked`           | updated  |          | Whether the token is already revoked |
| `active`            | updated  |          | Whether the token is still active    |
| `user_id`           | updated  |          | ID of associated user                |
| `user_name`         | updated  |          | Name of associated user              |

### Runner authentication tokens

See the [API description][runner-api] for details.

Token creation is not supported.

| name                | update   | validate | description                    |
|---------------------|----------|----------|--------------------------------|
| (secret)            | required | required | secret token                   |
| `token_type`        | required | required | `gitlab_token`                 |
| `gitlab_token_type` | required | required | `runner_authentication_token`  |
| `instance_url`      | required | required | GitLab instance URL            |
| `token_id`          | updated  |          | Group token ID                 |
| `expires_at`        | updated  |          | ISO8601 expiry date (optional) |

## Configuration via environment variables

| Name                | Secret | Required | Description                                                                                                |
|---------------------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `GITLAB_TOKENS`     | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens                                      |
| `GITLAB_TOKEN`      | yes    | yes      | GitLab private tokens as configured in `GITLAB_TOKENS` above                                               |
| `CKI_LOGGING_LEVEL` | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |

[project-api]: https://docs.gitlab.com/ee/api/project_access_tokens.html
[group-api]: https://docs.gitlab.com/ee/api/group_access_tokens.html
[personal-api]: https://docs.gitlab.com/ee/api/personal_access_tokens.html
[runner-api]: https://docs.gitlab.com/ee/api/runners.html
